﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class PhieuThuTienPhatDAO
    {
        public DataTable LayDSDocGiaNo()
        {
            DataTable dt = new DataTable();
            Provider pro = new Provider();
            try
            {
                pro.Connect();
                string strSql = "select pm.MaDG, pt.MaPT, pt.TienNo from PHIEUTRASACH pt join PHIEUMUONSACH pm on pt.MaPM=pm.MaPM where pt.TienNo > 0";
                dt = pro.Select(CommandType.Text, strSql);
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                pro.DisConnect();
            }
            return dt;
        }
        public int UpDatePhieuTra(PhieuThuTienPhatDTO pttp)
        {
            int nRow = 0;
            Provider pro = new Provider();
            try
            {
                string strSql = "UPDATE PHIEUTRASACH set TienNo = @TienNo WHERE PHIEUTRASACH.MaPT=@MaPT";
                pro.Connect();
                nRow = pro.ExecuteNonQuery(CommandType.Text, strSql, new SqlParameter { ParameterName = "@MaPT", Value = pttp.MaPT },
                                                                    new SqlParameter { ParameterName = "@TienNo", Value = pttp.TienNo });
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                pro.DisConnect();
            }
            return nRow;
        }
        public DataTable LayMaPhieuThu()
        {
            Provider pro = new Provider();
            DataTable dt = new DataTable();
            try
            {
                pro.Connect();
                string strSql = "SELECT TOP 1 MaPhieuThu FROM PHIEUTHUTIENPHAT order by MaPhieuThu Desc";
                dt = pro.Select(CommandType.Text, strSql);
                return dt;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                pro.DisConnect();
            }
        }
        public int ThemPhieuThuTienPhat(PhieuThuTienPhatDTO pttp)
        {
            int nRow = 0;
            Provider pro = new Provider();
            try
            {
                pro.Connect();
                string strSql = "insert into PhieuThuTienPhat values(@MaPhieuThu,@MaNV,@TienThu,@NgayThu,@MaPT,@TienConLai)";
                nRow = pro.ExecuteNonQuery(CommandType.Text, strSql, new SqlParameter { ParameterName = "@MaPhieuThu", Value = pttp.MaPhieuThu },
                                                                                new SqlParameter { ParameterName = "@MaNV", Value = pttp.MaNV },
                                                                                new SqlParameter { ParameterName = "@TienThu", Value = pttp.TienThu },
                                                                                new SqlParameter { ParameterName = "@NgayThu", Value = pttp.NgayThu },
                                                                                new SqlParameter { ParameterName = "@MaPT", Value = pttp.MaPT },
                                                                                new SqlParameter { ParameterName = "@TienConLai", Value = pttp.TienConLai }
                                                                     );
                return nRow;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                pro.DisConnect();
            }
        }
        public DataTable DanhSachNhanVienThuQuy()
        {
            Provider pro = new Provider();
            DataTable dt = new DataTable();
            try
            {
                pro.Connect();
                string strSql = "SELECT MaNhanVien FROM NHANVIEN WHERE MaBoPhan='BP04'";
                dt = pro.Select(CommandType.Text, strSql);
                return dt;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                pro.DisConnect();
            }
        }
    }
}
