﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    using System;
    using System.Collections.Generic;
    using System.Data;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    namespace DAO.SachDAO
    {
        public class TheLoaiDAO
        {
            public DataTable LayDanhSachTheLoai()
            {
                Provider provider = new Provider();
                try
                {
                    provider.Connect();
                    string query = "select * from TheLoaiSach";
                    DataTable table = provider.Select(CommandType.Text, query);
                    return table;
                }
                catch (Exception ex)
                {

                    throw ex;
                }
                finally
                {
                    provider.DisConnect();
                }
            }

            public void ThemTheLoai(string maTheLoai, string tenTheLoai)
            {
                Provider provider = new Provider();
                try
                {
                    provider.Connect();
                    string query = "INSERT INTO TheLoaiSach " +
                        "VALUES(@MaTheLoai, @TenTheLoai)";
                    provider.ExecuteNonQuery(CommandType.Text, query,
                        new SqlParameter { ParameterName = "@MaTheLoai", Value = maTheLoai },
                        new SqlParameter { ParameterName = "@TenTheLoai", Value = tenTheLoai });
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    provider.DisConnect();
                }
            }

            public void SuaTheLoai(string maTheLoai, string tenTheLoai)
            {
                Provider provider = new Provider();
                try
                {
                    provider.Connect();
                    string query = "Update TheLoaiSach set TenTheLoai = @TenTheLoai where MaTheLoai = @MaTheLoai";
                    provider.ExecuteNonQuery(CommandType.Text, query,
                        new SqlParameter { ParameterName = "@MaTheLoai", Value = maTheLoai },
                        new SqlParameter { ParameterName = "@TenTheLoai", Value = tenTheLoai });
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    provider.DisConnect();
                }
            }
            public void XoaTheLoai(string maTheLoai)
            {
                Provider provider = new Provider();
                try
                {
                    provider.Connect();
                    string query = "Delete TheLoaiSach where MaTheLoai = @MaTheLoai";
                    provider.ExecuteNonQuery(CommandType.Text, query,
                        new SqlParameter { ParameterName = "@MaTheLoai", Value = maTheLoai });
                }
                catch (Exception ex)
                {
                    throw ex;
                }
                finally
                {
                    provider.DisConnect();
                }
            }
        }
    }

}
