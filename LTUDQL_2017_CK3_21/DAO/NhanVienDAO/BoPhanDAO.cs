﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class BoPhanDAO
    {
		public DataTable LayDanhSachBoPhan()
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "select * from BoPhan";
				DataTable table = provider.Select(CommandType.Text, query);
				return table;
			}
			catch (Exception ex)
			{

				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}

		public void ThemBoPhan(string tenBoPhan, string maBoPhan)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "INSERT INTO BoPhan " +
					"VALUES(@MaBoPhan, @TenBoPhan)";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaBoPhan", Value = maBoPhan },
					new SqlParameter { ParameterName = "@TenBoPhan", Value = tenBoPhan });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}

		public void SuaBoPhan(string tenBoPhan, string maBoPhan)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Update BoPhan set TenBoPhan = @TenBoPhan where MaBoPhan = @MaBoPhan";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaBoPhan", Value = maBoPhan },
					new SqlParameter { ParameterName = "@TenBoPhan", Value = tenBoPhan });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
		public void XoaBoPhan(string maBoPhan)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Delete BoPhan where MaBoPhan = @MaBoPhan";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaBoPhan", Value = maBoPhan });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
	}
}
