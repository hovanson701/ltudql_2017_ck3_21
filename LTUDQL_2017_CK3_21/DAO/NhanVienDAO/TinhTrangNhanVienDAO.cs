﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class TinhTrangNhanVienDAO
    {
		public DataTable LayDanhSachTinhTrangNhanVien()
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "select * from TinhTrangNhanVien";
				DataTable table = provider.Select(CommandType.Text, query);
				return table;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}


		public void ThemTinhTrang(string tenTinhTrang, string maTinhTrang)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "INSERT INTO TinhTrangNhanVien " +
					"VALUES(@MaTinhTrang, @LoaiTinhTrang)";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaTinhTrang", Value = maTinhTrang },
					new SqlParameter { ParameterName = "@LoaiTinhTrang", Value = tenTinhTrang });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}

		public void SuaTinhTrang(string tenTinhTrang, string maTinhTrang)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Update TinhTrangNhanVien set LoaiTinhTrang = @LoaiTinhTrang where MaTinhTrangNV = @MaTinhTrang";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaTinhTrang", Value = maTinhTrang },
					new SqlParameter { ParameterName = "@LoaiTinhTrang", Value = tenTinhTrang });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
		public void XoaTinhTrang(string maTinhTrang)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Delete TinhTrangNhanVien where MaTinhTrangNV = @MaTinhTrang";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaTinhTrang", Value = maTinhTrang });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
	}
}
