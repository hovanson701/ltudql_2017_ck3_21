﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class ChucVuDAO
    {
		public DataTable LayDanhSachChucVu()
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "select * from ChucVu";
				DataTable table = provider.Select(CommandType.Text, query);
				return table;
			}
			catch (Exception ex)
			{

				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}


		public void ThemChucVu(string tenChucVu, string maChucVu)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "INSERT INTO ChucVu " +
					"VALUES(@MaChucVu, @TenChucVu)";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaChucVu", Value = maChucVu },
					new SqlParameter { ParameterName = "@TenChucVu", Value = tenChucVu });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}

		public void SuaChucVu(string tenChucVu, string maChucVu)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Update ChucVu set TenChucVu = @TenChucVu where MaChucVu = @MaChucVu";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaChucVu", Value = maChucVu },
					new SqlParameter { ParameterName = "@TenChucVu", Value = tenChucVu });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
		public void XoaChucVu(string maChucVu)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Delete ChucVu where MaChucVu = @MaChucVu";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaChucVu", Value = maChucVu });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
	}
}
