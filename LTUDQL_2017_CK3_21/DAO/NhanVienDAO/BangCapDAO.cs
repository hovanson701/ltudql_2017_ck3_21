﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class BangCapDAO
    {
		public DataTable LayDanhSachBangCap()
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "select * from BangCap";
				DataTable table = provider.Select(CommandType.Text, query);
				return table;
			}
			catch (Exception ex)
			{

				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}

		public void ThemBangCap(string tenBangCap, string maBangCap)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "INSERT INTO BangCap " +
					"VALUES(@MaBangCap, @LoaiBangCap)";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaBangCap", Value = maBangCap},
					new SqlParameter { ParameterName = "@LoaiBangCap", Value = tenBangCap});
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}

		public void SuaBangCap(string tenBangCap, string maBangCap)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Update BangCap set LoaiBangCap = @LoaiBangCap where MaBangCap = @MaBangCap" ;
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaBangCap", Value = maBangCap },
					new SqlParameter { ParameterName = "@LoaiBangCap", Value = tenBangCap });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
		public void XoaBangCap( string maBangCap)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Delete BangCap where MaBangCap = @MaBangCap";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaBangCap", Value = maBangCap });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
	}
}
