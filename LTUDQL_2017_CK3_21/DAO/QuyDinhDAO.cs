﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class QuyDinhDAO
    {
		public DataTable LayDanhSachQuyDinh()
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "select * from QuyDinh ";
				DataTable table = provider.Select(CommandType.Text, query);
				return table;
			}
			catch (Exception ex)
			{

				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}

		public void CapNhatQuyDinh(DataTable dtQuyDinh)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "select * from QuyDinh";
				provider.AdapterUpdate(query, dtQuyDinh);
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
	}
}
