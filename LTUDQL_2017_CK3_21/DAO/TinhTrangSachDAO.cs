﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class TinhTrangSachDAO
    {
        public DataTable LayDanhSachTinhTrangSach()
        {
            Provider provider = new Provider();
            try
            {
                provider.Connect();
                string query = "select * from TinhTrangSach";
                DataTable table = provider.Select(CommandType.Text, query);
                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
        }

        public void ThemTinhTrangSach(string maTTS, string tenTTS)
        {
            Provider provider = new Provider();
            try
            {
                provider.Connect();
                string query = "INSERT INTO TinhTrangSach " +
                    "VALUES(@MaTTS, @TenTTS)";
                provider.ExecuteNonQuery(CommandType.Text, query,
                    new SqlParameter { ParameterName = "@MaTTS", Value = maTTS },
                    new SqlParameter { ParameterName = "@TenTTS", Value = tenTTS });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
        }

        public void SuaTinhTrangSach(string maTTS, string tenTTS)
        {
            Provider provider = new Provider();
            try
            {
                provider.Connect();
                string query = "Update TinhTrangSach set TenTinhTrang = @TenTTS where MaTTSach = @MaTTS";
                provider.ExecuteNonQuery(CommandType.Text, query,
                    new SqlParameter { ParameterName = "@MaTTS", Value = maTTS },
                    new SqlParameter { ParameterName = "@TenTTS", Value = tenTTS });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
        }

        public void XoaTinhTrangSach(string maTTS)
        {
            Provider provider = new Provider();
            try
            {
                provider.Connect();
                string query = "Delete TinhTrangSach where MaTTSach = @MaTTS";
                provider.ExecuteNonQuery(CommandType.Text, query,
                    new SqlParameter { ParameterName = "@MaTTS", Value = maTTS });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
        }
    }
}
