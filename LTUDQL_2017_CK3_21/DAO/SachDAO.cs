﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class SachDAO
    {
        // Kiểm tra mã sách đã tồn tại trước đó hay chưa
        public static bool WasExistBookId(string bookId)
        {
            Provider p = new Provider();
            p.Connect();

            try
            {
                string cmd = "SELECT * from SACH where MaSach = '" + bookId + "'";
                var dt = p.Select(System.Data.CommandType.Text, cmd);
                return dt.Rows.Count != 0; // Chưa có
            }
            catch(SqlException ex)
            {
                throw ex;
            }
            finally
            {
                p.DisConnect();
            }
        }

        // Lấy dữ liệu được truy vấn bằng text command
        public static DataTable GetDatasByCommand(string cmd)
        {
            Provider provider = new Provider();
            try
            {
                provider.Connect(); ;
                DataTable dt = provider.Select(CommandType.Text, cmd);
                return dt;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
        }

        public static DataTable GetDatasByStoredProcedure(string cmd)
        {
            Provider provider = new Provider();
            try
            {
                provider.Connect(); ;
                DataTable dt = provider.Select(CommandType.StoredProcedure, cmd);
                return dt;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
        }

        // Thêm sách mới vào csdl
        public static int AddANewBook(DTO.Sach book)
        {
            int nRow = 0;
            Provider provider = new Provider();
            try
            {
                string strSql = "INSERT INTO SACH(MaSach, TenSach, MaTheLoai, MaTacGia, TTS, MaNXB, NgayXuatBan, GiaTien, Ke, NgayNhap, MaNVTiepNhan) " +
                    "VALUES(@Ma, @Ten, @MaTheLoai, @MaTacGia, @TTS, @MaNXB, @NgayXuatBan, @GiaTien, @Ke, @NgayNhap, @MaNVTN)";
                provider.Connect();
                nRow = provider.ExecuteNonQuery(CommandType.Text, strSql,
                            new SqlParameter { ParameterName = "@Ma", Value = book.Ma },
                            new SqlParameter { ParameterName = "@Ten", Value = book.Ten },
                            new SqlParameter { ParameterName = "@MaTheLoai", Value = book.MaTheLoai },
                            new SqlParameter { ParameterName = "@MaTacGia", Value = book.MaTacGia },
                            new SqlParameter { ParameterName = "@TTS", Value = "TTS02" },
                             new SqlParameter { ParameterName = "@MaNXB", Value = book.MaMXB },
                              new SqlParameter { ParameterName = "@NgayXuatBan", Value = book.NgayXuatBan},
                               new SqlParameter { ParameterName = "@Giatien", Value = book.GiaTien },
                                new SqlParameter { ParameterName = "@Ke", Value = book.ViTri },
                                 new SqlParameter { ParameterName = "@NgayNhap", Value = book.NgayTiepNhan},
                                 new SqlParameter { ParameterName = "@MaNVTN", Value = book.MaNVTiepNhan }
                    );

            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
            return nRow;
        }

        // Tìm sách theo các tiêu chí
        public static  DataTable FindBook(DTO.Sach book)
        {
            Provider provider = new Provider();

            try
            {
                provider.Connect();
                string cmd = "USP_FindBook";
                DataTable dt = provider.Select(CommandType.StoredProcedure, cmd,
                                    new SqlParameter { ParameterName = "@name", Value = book.Ten },
                                    new SqlParameter { ParameterName = "@category_id", Value = book.MaTheLoai },
                                    new SqlParameter { ParameterName = "@author_id", Value = book.MaTacGia },
                                    new SqlParameter { ParameterName = "@publisher_id", Value = book.MaMXB });
                return dt;
            }
            catch(SqlException ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
        }
    }
}
