﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class TacGiaDAO
    {
        public DataTable LayDanhSachTacGia()
        {
            Provider provider = new Provider();
            try
            {
                provider.Connect();
                string query = "select * from TACGIA";
                DataTable table = provider.Select(CommandType.Text, query);
                return table;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
        }

        public void ThemTacGia(string maTacGia, string tenTacGia)
        {
            Provider provider = new Provider();
            try
            {
                provider.Connect();
                string query = "INSERT INTO TACGIA " +
                    "VALUES(@MaTacGia, @TenTacGia)";
                provider.ExecuteNonQuery(CommandType.Text, query,
                    new SqlParameter { ParameterName = "@MaTacGia", Value = maTacGia },
                    new SqlParameter { ParameterName = "@TenTacGia", Value = tenTacGia });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
        }

        public void SuaTacGia(string maTacGia, string tenTacGia)
        {
            Provider provider = new Provider();
            try
            {
                provider.Connect();
                string query = "Update TACGIA set TenTacGia = @TenTacGia where MaTacGia = @MaTacGia";
                provider.ExecuteNonQuery(CommandType.Text, query,
                    new SqlParameter { ParameterName = "@MaTacGia", Value = maTacGia },
                    new SqlParameter { ParameterName = "@TenTacGia", Value = tenTacGia });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
        }
        public void XoaTacGia(string maTacGia)
        {
            Provider provider = new Provider();
            try
            {
                provider.Connect();
                string query = "Delete TACGIA where MaTacGia = @MaTacGia";
                provider.ExecuteNonQuery(CommandType.Text, query,
                    new SqlParameter { ParameterName = "@MaTacGia", Value = maTacGia });
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                provider.DisConnect();
            }
        }
    }
}
