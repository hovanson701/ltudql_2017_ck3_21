﻿using DTO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO
{
    public class GhiNhanMatSachDAO
    {
        public DataTable MaGhiNhanSachMat()
        {
            Provider pro = new Provider();
            DataTable dt = new DataTable();
            try
            {
                pro.Connect();
                string strSql = "SELECT MaGNMatSach FROM MATSACH ORDER BY MaGNMatSach DESC";
                dt = pro.Select(CommandType.Text, strSql);
                return dt;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                pro.DisConnect();
            }
        }
        public DataTable DanhSachNhanVienThuThu()
        {
            Provider pro = new Provider();
            DataTable dt = new DataTable();
            try
            {
                pro.Connect();
                string strSql = "SELECT MaNhanVien FROM NHANVIEN WHERE MaBoPhan='BP02'";
                dt = pro.Select(CommandType.Text, strSql);
                return dt;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                pro.DisConnect();
            }
        }
        public int CapNhatPhieuTra(GhiNhanMatSachDTO gnms)
        {
            int nRow = 0;
            Provider pro = new Provider();
            try
            {
                string strSql = "SP_CapNhatTienPhat";
                pro.Connect();
                nRow = pro.ExecuteNonQuery(CommandType.StoredProcedure, strSql, new SqlParameter { ParameterName = "@MaPT", Value = gnms.MaPT },
                                                                    new SqlParameter { ParameterName = "@TienNo", Value = gnms.TienNo },
                                                                    new SqlParameter { ParameterName = "@MaPM", Value = gnms.MaPM }
                                                                    );
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                pro.DisConnect();
            }
            return nRow;
        }
        public int ThemSachMat(GhiNhanMatSachDTO gnms)
        {
            Provider pro = new Provider();
            int nRow = 0;
            try
            {
                pro.Connect();
                string strSql = "Insert into MATSACH values(@MaGNMatSach,@MaSach,@NgayGhiNhan,@MaDG,@TienPhat,@MaNV)";
                nRow = pro.ExecuteNonQuery(CommandType.Text, strSql, new SqlParameter { ParameterName = "@MaGNMatSach", Value = gnms.MaGNMatSach },
                                                                                new SqlParameter { ParameterName = "@MaSach", Value = gnms.MaSach },
                                                                                new SqlParameter { ParameterName = "@NgayGhiNhan", Value = gnms.NgayGhiNhan },
                                                                                new SqlParameter { ParameterName = "@MaDG", Value = gnms.MaDG },
                                                                                new SqlParameter { ParameterName = "@TienPhat", Value = gnms.TienPhat },
                                                                                new SqlParameter { ParameterName = "@MaNV", Value = gnms.MaNV }
                                                                                );

                return nRow;

            }
            catch (SqlException ex)
            {
                throw ex;
            }

        }
        public DataTable SachDocGia()
        {

            Provider pro = new Provider();
            DataTable dt = new DataTable();
            try
            {
                pro.Connect();
                string strSql = "SELECT pm.MaPM, pm.MaDG, ctpm.MaSach, s.GiaTien FROM CT_PHIEUMUON ctpm JOIN PHIEUMUONSACH pm ON ctpm.MaPM=pm.MaPM JOIN SACH s ON s.MaSach=ctpm.MaSach";
                dt = pro.Select(CommandType.Text, strSql);
                return dt;
            }
            catch (SqlException ex)
            {
                throw ex;
            }
            finally
            {
                pro.DisConnect();
            }
        }
    }
}
