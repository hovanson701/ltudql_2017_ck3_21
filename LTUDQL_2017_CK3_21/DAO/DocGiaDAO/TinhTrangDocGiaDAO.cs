﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.DocGiaDAO
{
    public class TinhTrangDocGiaDAO
    {
		public DataTable LayDanhSachTinhTrangDocGia()
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "select * from TinhTrangDocGia";
				DataTable table = provider.Select(CommandType.Text, query);
				return table;
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}

		public void ThemTinhTrangDG(string tenTinhTrangDG, string maTinhTrangDG)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "INSERT INTO TinhTrangDocGia " +
					"VALUES(@MaTinhTrangDG, @LoaiTinhTrangDG)";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaTinhTrangDG", Value = maTinhTrangDG },
					new SqlParameter { ParameterName = "@LoaiTinhTrangDG", Value = tenTinhTrangDG });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}

		public void SuaTinhTrangDG(string tenTinhTrangDG, string maTinhTrangDG)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Update TinhTrangDocGia set LoaiTinhTrangDG = @LoaiTinhTrangDG where MaTinhTrangDG = @MaTinhTrangDG";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaTinhTrangDG", Value = maTinhTrangDG },
					new SqlParameter { ParameterName = "@LoaiTinhTrangDG", Value = tenTinhTrangDG });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
		public void XoaTinhTrangDG(string maTinhTrangDG)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Delete TinhTrangDocGia where MaTinhTrangDG = @MaTinhTrangDG";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaTinhTrangDG", Value = maTinhTrangDG });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
	}
}
