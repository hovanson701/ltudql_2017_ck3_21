﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DAO.DocGiaDAO
{
    public class LoaiDocGiaDAO
    {

		public DataTable LayDanhSachLoaiDocGia()
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "select * from LoaiDocGia";
				DataTable table = provider.Select(CommandType.Text, query);
				return table;
			}
			catch (Exception ex)
			{

				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}

		public void ThemLoaiDocGia(string tenLoaiDocGia, string maLoaiDocGia)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "INSERT INTO LoaiDocGia " +
					"VALUES(@MaLoaiDocGia, @LoaiLoaiDocGia)";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaLoaiDocGia", Value = maLoaiDocGia },
					new SqlParameter { ParameterName = "@LoaiLoaiDocGia", Value = tenLoaiDocGia });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}

		public void SuaLoaiDocGia(string tenLoaiDocGia, string maLoaiDocGia)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Update LoaiDocGia set TenLoaiDG = @TenLoaiDG where MaLoaiDG = @MaLoaiDG";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaLoaiDG", Value = maLoaiDocGia },
					new SqlParameter { ParameterName = "@TenLoaiDG", Value = tenLoaiDocGia });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
		public void XoaLoaiDocGia(string maLoaiDocGia)
		{
			Provider provider = new Provider();
			try
			{
				provider.Connect();
				string query = "Delete LoaiDocGia where MaLoaiDG = @MaLoaiDocGia";
				provider.ExecuteNonQuery(CommandType.Text, query,
					new SqlParameter { ParameterName = "@MaLoaiDocGia", Value = maLoaiDocGia });
			}
			catch (Exception ex)
			{
				throw ex;
			}
			finally
			{
				provider.DisConnect();
			}
		}
	}
}
