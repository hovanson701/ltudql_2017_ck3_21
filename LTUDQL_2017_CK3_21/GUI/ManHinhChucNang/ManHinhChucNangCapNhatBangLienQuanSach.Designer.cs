﻿namespace GUI.ManHinhChucNang
{
    partial class ManHinhChucNangCapNhatBangLienQuanSach
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dtgridviewTheLoaiSach = new System.Windows.Forms.DataGridView();
            this.MaTheLoai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenTheLoai = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnThemTheLoai = new System.Windows.Forms.Button();
            this.btnXoaTL = new System.Windows.Forms.Button();
            this.btnSuaTheLoai = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.txtMaTL = new System.Windows.Forms.TextBox();
            this.txtTenTheLoai = new System.Windows.Forms.TextBox();
            this.txtTenTG = new System.Windows.Forms.TextBox();
            this.txtMaTG = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnSuaTacGia = new System.Windows.Forms.Button();
            this.btnXoaTG = new System.Windows.Forms.Button();
            this.btnThemTacGia = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.dtgridviewTacGia = new System.Windows.Forms.DataGridView();
            this.MaTacGia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenTacGia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label8 = new System.Windows.Forms.Label();
            this.txtTenNXB = new System.Windows.Forms.TextBox();
            this.txtMaNXB = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.btnSuaNXB = new System.Windows.Forms.Button();
            this.btnXoaNXB = new System.Windows.Forms.Button();
            this.btnThemNXB = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.dtgridviewNXB = new System.Windows.Forms.DataGridView();
            this.MaNXB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenNXB = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label12 = new System.Windows.Forms.Label();
            this.txtTenTTS = new System.Windows.Forms.TextBox();
            this.txtMaTTS = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.btnSuaTinhTrang = new System.Windows.Forms.Button();
            this.btnXoaTinhTrang = new System.Windows.Forms.Button();
            this.btnThemTinhTrang = new System.Windows.Forms.Button();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.dtgridviewTTS = new System.Windows.Forms.DataGridView();
            this.MaTTSach = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenTinhTrang = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label16 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dtgridviewTheLoaiSach)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgridviewTacGia)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgridviewNXB)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgridviewTTS)).BeginInit();
            this.SuspendLayout();
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(113, 18);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(454, 29);
            this.label.TabIndex = 0;
            this.label.Text = "Cập nhật các bảng liên quan đến sách";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 68);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 17);
            this.label1.TabIndex = 1;
            this.label1.Text = "Thể loại sách";
            // 
            // dtgridviewTheLoaiSach
            // 
            this.dtgridviewTheLoaiSach.AllowUserToAddRows = false;
            this.dtgridviewTheLoaiSach.AllowUserToDeleteRows = false;
            this.dtgridviewTheLoaiSach.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgridviewTheLoaiSach.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgridviewTheLoaiSach.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaTheLoai,
            this.TenTheLoai});
            this.dtgridviewTheLoaiSach.Location = new System.Drawing.Point(16, 87);
            this.dtgridviewTheLoaiSach.Name = "dtgridviewTheLoaiSach";
            this.dtgridviewTheLoaiSach.ReadOnly = true;
            this.dtgridviewTheLoaiSach.RowHeadersVisible = false;
            this.dtgridviewTheLoaiSach.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgridviewTheLoaiSach.Size = new System.Drawing.Size(214, 207);
            this.dtgridviewTheLoaiSach.TabIndex = 2;
            this.dtgridviewTheLoaiSach.SelectionChanged += new System.EventHandler(this.dtgridviewTheLoaiSach_SelectionChanged);
            // 
            // MaTheLoai
            // 
            this.MaTheLoai.DataPropertyName = "MaTheLoai";
            this.MaTheLoai.HeaderText = "Mã thể loại";
            this.MaTheLoai.Name = "MaTheLoai";
            this.MaTheLoai.ReadOnly = true;
            // 
            // TenTheLoai
            // 
            this.TenTheLoai.DataPropertyName = "TenTheLoai";
            this.TenTheLoai.HeaderText = "Tên thể loại";
            this.TenTheLoai.Name = "TenTheLoai";
            this.TenTheLoai.ReadOnly = true;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(297, 281);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(238, 213);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 4;
            this.label3.Text = "Mã thể loại";
            // 
            // btnThemTheLoai
            // 
            this.btnThemTheLoai.Location = new System.Drawing.Point(241, 87);
            this.btnThemTheLoai.Name = "btnThemTheLoai";
            this.btnThemTheLoai.Size = new System.Drawing.Size(98, 37);
            this.btnThemTheLoai.TabIndex = 5;
            this.btnThemTheLoai.Text = "Thêm thể loại";
            this.btnThemTheLoai.UseVisualStyleBackColor = true;
            this.btnThemTheLoai.Click += new System.EventHandler(this.btnThemTheLoai_Click);
            // 
            // btnXoaTL
            // 
            this.btnXoaTL.Location = new System.Drawing.Point(241, 130);
            this.btnXoaTL.Name = "btnXoaTL";
            this.btnXoaTL.Size = new System.Drawing.Size(98, 37);
            this.btnXoaTL.TabIndex = 6;
            this.btnXoaTL.Text = "Xóa thể loại";
            this.btnXoaTL.UseVisualStyleBackColor = true;
            this.btnXoaTL.Click += new System.EventHandler(this.btnXoaTL_Click);
            // 
            // btnSuaTheLoai
            // 
            this.btnSuaTheLoai.Location = new System.Drawing.Point(241, 173);
            this.btnSuaTheLoai.Name = "btnSuaTheLoai";
            this.btnSuaTheLoai.Size = new System.Drawing.Size(98, 37);
            this.btnSuaTheLoai.TabIndex = 7;
            this.btnSuaTheLoai.Text = "Sửa thể loại";
            this.btnSuaTheLoai.UseVisualStyleBackColor = true;
            this.btnSuaTheLoai.Click += new System.EventHandler(this.btnSuaTheLoai_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(238, 256);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 17);
            this.label4.TabIndex = 8;
            this.label4.Text = "Tên thể loại";
            // 
            // txtMaTL
            // 
            this.txtMaTL.Location = new System.Drawing.Point(241, 233);
            this.txtMaTL.Name = "txtMaTL";
            this.txtMaTL.ReadOnly = true;
            this.txtMaTL.Size = new System.Drawing.Size(98, 20);
            this.txtMaTL.TabIndex = 9;
            // 
            // txtTenTheLoai
            // 
            this.txtTenTheLoai.Location = new System.Drawing.Point(241, 276);
            this.txtTenTheLoai.Name = "txtTenTheLoai";
            this.txtTenTheLoai.Size = new System.Drawing.Size(98, 20);
            this.txtTenTheLoai.TabIndex = 10;
            // 
            // txtTenTG
            // 
            this.txtTenTG.Location = new System.Drawing.Point(578, 276);
            this.txtTenTG.Name = "txtTenTG";
            this.txtTenTG.Size = new System.Drawing.Size(98, 20);
            this.txtTenTG.TabIndex = 20;
            // 
            // txtMaTG
            // 
            this.txtMaTG.Location = new System.Drawing.Point(578, 233);
            this.txtMaTG.Name = "txtMaTG";
            this.txtMaTG.ReadOnly = true;
            this.txtMaTG.Size = new System.Drawing.Size(98, 20);
            this.txtMaTG.TabIndex = 19;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(575, 256);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(79, 17);
            this.label5.TabIndex = 18;
            this.label5.Text = "Tên tác giả";
            // 
            // btnSuaTacGia
            // 
            this.btnSuaTacGia.Location = new System.Drawing.Point(578, 173);
            this.btnSuaTacGia.Name = "btnSuaTacGia";
            this.btnSuaTacGia.Size = new System.Drawing.Size(98, 37);
            this.btnSuaTacGia.TabIndex = 17;
            this.btnSuaTacGia.Text = "Sửa thể loại";
            this.btnSuaTacGia.UseVisualStyleBackColor = true;
            this.btnSuaTacGia.Click += new System.EventHandler(this.btnSuaTacGia_Click);
            // 
            // btnXoaTG
            // 
            this.btnXoaTG.Location = new System.Drawing.Point(578, 130);
            this.btnXoaTG.Name = "btnXoaTG";
            this.btnXoaTG.Size = new System.Drawing.Size(98, 37);
            this.btnXoaTG.TabIndex = 16;
            this.btnXoaTG.Text = "Xóa tác giả";
            this.btnXoaTG.UseVisualStyleBackColor = true;
            this.btnXoaTG.Click += new System.EventHandler(this.btnXoaTG_Click);
            // 
            // btnThemTacGia
            // 
            this.btnThemTacGia.Location = new System.Drawing.Point(578, 87);
            this.btnThemTacGia.Name = "btnThemTacGia";
            this.btnThemTacGia.Size = new System.Drawing.Size(98, 37);
            this.btnThemTacGia.TabIndex = 15;
            this.btnThemTacGia.Text = "Thêm tác giả";
            this.btnThemTacGia.UseVisualStyleBackColor = true;
            this.btnThemTacGia.Click += new System.EventHandler(this.btnThemTacGia_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(575, 213);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 17);
            this.label6.TabIndex = 14;
            this.label6.Text = "Mã tác giả";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(634, 281);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(0, 13);
            this.label7.TabIndex = 13;
            // 
            // dtgridviewTacGia
            // 
            this.dtgridviewTacGia.AllowUserToAddRows = false;
            this.dtgridviewTacGia.AllowUserToDeleteRows = false;
            this.dtgridviewTacGia.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgridviewTacGia.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgridviewTacGia.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaTacGia,
            this.TenTacGia});
            this.dtgridviewTacGia.Location = new System.Drawing.Point(353, 87);
            this.dtgridviewTacGia.Name = "dtgridviewTacGia";
            this.dtgridviewTacGia.ReadOnly = true;
            this.dtgridviewTacGia.RowHeadersVisible = false;
            this.dtgridviewTacGia.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgridviewTacGia.Size = new System.Drawing.Size(214, 207);
            this.dtgridviewTacGia.TabIndex = 12;
            this.dtgridviewTacGia.SelectionChanged += new System.EventHandler(this.dtgridviewTacGia_SelectionChanged);
            // 
            // MaTacGia
            // 
            this.MaTacGia.DataPropertyName = "MaTacGia";
            this.MaTacGia.HeaderText = "Mã tác giả";
            this.MaTacGia.Name = "MaTacGia";
            this.MaTacGia.ReadOnly = true;
            // 
            // TenTacGia
            // 
            this.TenTacGia.DataPropertyName = "TenTacGia";
            this.TenTacGia.HeaderText = "Tên tác giả";
            this.TenTacGia.Name = "TenTacGia";
            this.TenTacGia.ReadOnly = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(350, 68);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 17);
            this.label8.TabIndex = 11;
            this.label8.Text = "Tác giả";
            // 
            // txtTenNXB
            // 
            this.txtTenNXB.Location = new System.Drawing.Point(241, 509);
            this.txtTenNXB.Name = "txtTenNXB";
            this.txtTenNXB.Size = new System.Drawing.Size(98, 20);
            this.txtTenNXB.TabIndex = 30;
            // 
            // txtMaNXB
            // 
            this.txtMaNXB.Location = new System.Drawing.Point(241, 466);
            this.txtMaNXB.Name = "txtMaNXB";
            this.txtMaNXB.ReadOnly = true;
            this.txtMaNXB.Size = new System.Drawing.Size(98, 20);
            this.txtMaNXB.TabIndex = 29;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(238, 489);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(65, 17);
            this.label9.TabIndex = 28;
            this.label9.Text = "Tên NXB";
            // 
            // btnSuaNXB
            // 
            this.btnSuaNXB.Location = new System.Drawing.Point(241, 406);
            this.btnSuaNXB.Name = "btnSuaNXB";
            this.btnSuaNXB.Size = new System.Drawing.Size(98, 37);
            this.btnSuaNXB.TabIndex = 27;
            this.btnSuaNXB.Text = "Sửa NXB";
            this.btnSuaNXB.UseVisualStyleBackColor = true;
            this.btnSuaNXB.Click += new System.EventHandler(this.btnSuaNXB_Click);
            // 
            // btnXoaNXB
            // 
            this.btnXoaNXB.Location = new System.Drawing.Point(241, 363);
            this.btnXoaNXB.Name = "btnXoaNXB";
            this.btnXoaNXB.Size = new System.Drawing.Size(98, 37);
            this.btnXoaNXB.TabIndex = 26;
            this.btnXoaNXB.Text = "Xóa NXB";
            this.btnXoaNXB.UseVisualStyleBackColor = true;
            this.btnXoaNXB.Click += new System.EventHandler(this.btnXoaNXB_Click);
            // 
            // btnThemNXB
            // 
            this.btnThemNXB.Location = new System.Drawing.Point(241, 320);
            this.btnThemNXB.Name = "btnThemNXB";
            this.btnThemNXB.Size = new System.Drawing.Size(98, 37);
            this.btnThemNXB.TabIndex = 25;
            this.btnThemNXB.Text = "Thêm NXB";
            this.btnThemNXB.UseVisualStyleBackColor = true;
            this.btnThemNXB.Click += new System.EventHandler(this.btnThemNXB_Click);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(238, 446);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 17);
            this.label10.TabIndex = 24;
            this.label10.Text = "Mã NXB";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(297, 514);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(0, 13);
            this.label11.TabIndex = 23;
            // 
            // dtgridviewNXB
            // 
            this.dtgridviewNXB.AllowUserToAddRows = false;
            this.dtgridviewNXB.AllowUserToDeleteRows = false;
            this.dtgridviewNXB.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgridviewNXB.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgridviewNXB.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaNXB,
            this.TenNXB});
            this.dtgridviewNXB.Location = new System.Drawing.Point(16, 320);
            this.dtgridviewNXB.Name = "dtgridviewNXB";
            this.dtgridviewNXB.ReadOnly = true;
            this.dtgridviewNXB.RowHeadersVisible = false;
            this.dtgridviewNXB.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgridviewNXB.Size = new System.Drawing.Size(214, 207);
            this.dtgridviewNXB.TabIndex = 22;
            this.dtgridviewNXB.SelectionChanged += new System.EventHandler(this.dtgridviewNXB_SelectionChanged);
            // 
            // MaNXB
            // 
            this.MaNXB.DataPropertyName = "MaNXB";
            this.MaNXB.HeaderText = "Mã NXB";
            this.MaNXB.Name = "MaNXB";
            this.MaNXB.ReadOnly = true;
            // 
            // TenNXB
            // 
            this.TenNXB.DataPropertyName = "TenNXB";
            this.TenNXB.HeaderText = "Tên NXB";
            this.TenNXB.Name = "TenNXB";
            this.TenNXB.ReadOnly = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(13, 301);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(104, 17);
            this.label12.TabIndex = 21;
            this.label12.Text = "Nhà xuất bản";
            // 
            // txtTenTTS
            // 
            this.txtTenTTS.Location = new System.Drawing.Point(578, 509);
            this.txtTenTTS.Name = "txtTenTTS";
            this.txtTenTTS.Size = new System.Drawing.Size(98, 20);
            this.txtTenTTS.TabIndex = 40;
            // 
            // txtMaTTS
            // 
            this.txtMaTTS.Location = new System.Drawing.Point(578, 466);
            this.txtMaTTS.Name = "txtMaTTS";
            this.txtMaTTS.ReadOnly = true;
            this.txtMaTTS.Size = new System.Drawing.Size(98, 20);
            this.txtMaTTS.TabIndex = 39;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(575, 489);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(97, 17);
            this.label13.TabIndex = 38;
            this.label13.Text = "Tên tình trạng";
            // 
            // btnSuaTinhTrang
            // 
            this.btnSuaTinhTrang.Location = new System.Drawing.Point(578, 406);
            this.btnSuaTinhTrang.Name = "btnSuaTinhTrang";
            this.btnSuaTinhTrang.Size = new System.Drawing.Size(98, 37);
            this.btnSuaTinhTrang.TabIndex = 37;
            this.btnSuaTinhTrang.Text = "Sửa tình trạng";
            this.btnSuaTinhTrang.UseVisualStyleBackColor = true;
            this.btnSuaTinhTrang.Click += new System.EventHandler(this.btnSuaTinhTrang_Click);
            // 
            // btnXoaTinhTrang
            // 
            this.btnXoaTinhTrang.Location = new System.Drawing.Point(578, 363);
            this.btnXoaTinhTrang.Name = "btnXoaTinhTrang";
            this.btnXoaTinhTrang.Size = new System.Drawing.Size(98, 37);
            this.btnXoaTinhTrang.TabIndex = 36;
            this.btnXoaTinhTrang.Text = "Xóa tình trạng";
            this.btnXoaTinhTrang.UseVisualStyleBackColor = true;
            this.btnXoaTinhTrang.Click += new System.EventHandler(this.btnXoaTinhTrang_Click);
            // 
            // btnThemTinhTrang
            // 
            this.btnThemTinhTrang.Location = new System.Drawing.Point(578, 320);
            this.btnThemTinhTrang.Name = "btnThemTinhTrang";
            this.btnThemTinhTrang.Size = new System.Drawing.Size(98, 37);
            this.btnThemTinhTrang.TabIndex = 35;
            this.btnThemTinhTrang.Text = "Thêm tình trạng";
            this.btnThemTinhTrang.UseVisualStyleBackColor = true;
            this.btnThemTinhTrang.Click += new System.EventHandler(this.btnThemTinhTrang_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(575, 446);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(91, 17);
            this.label14.TabIndex = 34;
            this.label14.Text = "Mã tình trạng";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(634, 514);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(0, 13);
            this.label15.TabIndex = 33;
            // 
            // dtgridviewTTS
            // 
            this.dtgridviewTTS.AllowUserToAddRows = false;
            this.dtgridviewTTS.AllowUserToDeleteRows = false;
            this.dtgridviewTTS.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dtgridviewTTS.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dtgridviewTTS.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.MaTTSach,
            this.TenTinhTrang});
            this.dtgridviewTTS.Location = new System.Drawing.Point(353, 320);
            this.dtgridviewTTS.Name = "dtgridviewTTS";
            this.dtgridviewTTS.ReadOnly = true;
            this.dtgridviewTTS.RowHeadersVisible = false;
            this.dtgridviewTTS.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dtgridviewTTS.Size = new System.Drawing.Size(214, 207);
            this.dtgridviewTTS.TabIndex = 32;
            this.dtgridviewTTS.SelectionChanged += new System.EventHandler(this.dtgridviewTTS_SelectionChanged);
            // 
            // MaTTSach
            // 
            this.MaTTSach.DataPropertyName = "MaTTSach";
            this.MaTTSach.HeaderText = "Mã tình trạng";
            this.MaTTSach.Name = "MaTTSach";
            this.MaTTSach.ReadOnly = true;
            // 
            // TenTinhTrang
            // 
            this.TenTinhTrang.DataPropertyName = "TenTinhTrang";
            this.TenTinhTrang.HeaderText = "Tên tình trạng";
            this.TenTinhTrang.Name = "TenTinhTrang";
            this.TenTinhTrang.ReadOnly = true;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(350, 301);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(122, 17);
            this.label16.TabIndex = 31;
            this.label16.Text = "Tình trạng sách";
            // 
            // ManHinhChucNangCapNhatBangLienQuanSach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.Controls.Add(this.txtTenTTS);
            this.Controls.Add(this.txtMaTTS);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.btnSuaTinhTrang);
            this.Controls.Add(this.btnXoaTinhTrang);
            this.Controls.Add(this.btnThemTinhTrang);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.dtgridviewTTS);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.txtTenNXB);
            this.Controls.Add(this.txtMaNXB);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnSuaNXB);
            this.Controls.Add(this.btnXoaNXB);
            this.Controls.Add(this.btnThemNXB);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.dtgridviewNXB);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtTenTG);
            this.Controls.Add(this.txtMaTG);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSuaTacGia);
            this.Controls.Add(this.btnXoaTG);
            this.Controls.Add(this.btnThemTacGia);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.dtgridviewTacGia);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtTenTheLoai);
            this.Controls.Add(this.txtMaTL);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnSuaTheLoai);
            this.Controls.Add(this.btnXoaTL);
            this.Controls.Add(this.btnThemTheLoai);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dtgridviewTheLoaiSach);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label);
            this.Name = "ManHinhChucNangCapNhatBangLienQuanSach";
            this.Size = new System.Drawing.Size(680, 530);
            this.Load += new System.EventHandler(this.ManHinhChucNangCapNhatBangLienQuanSach_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dtgridviewTheLoaiSach)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgridviewTacGia)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgridviewNXB)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtgridviewTTS)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dtgridviewTheLoaiSach;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnThemTheLoai;
        private System.Windows.Forms.Button btnXoaTL;
        private System.Windows.Forms.Button btnSuaTheLoai;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtMaTL;
        private System.Windows.Forms.TextBox txtTenTheLoai;
        private System.Windows.Forms.TextBox txtTenTG;
        private System.Windows.Forms.TextBox txtMaTG;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnSuaTacGia;
        private System.Windows.Forms.Button btnXoaTG;
        private System.Windows.Forms.Button btnThemTacGia;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dtgridviewTacGia;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaTacGia;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenTacGia;
        private System.Windows.Forms.TextBox txtTenNXB;
        private System.Windows.Forms.TextBox txtMaNXB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSuaNXB;
        private System.Windows.Forms.Button btnXoaNXB;
        private System.Windows.Forms.Button btnThemNXB;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.DataGridView dtgridviewNXB;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtTenTTS;
        private System.Windows.Forms.TextBox txtMaTTS;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Button btnSuaTinhTrang;
        private System.Windows.Forms.Button btnXoaTinhTrang;
        private System.Windows.Forms.Button btnThemTinhTrang;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataGridView dtgridviewTTS;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaTTSach;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenTinhTrang;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaTheLoai;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenTheLoai;
        private System.Windows.Forms.DataGridViewTextBoxColumn MaNXB;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenNXB;
    }
}
