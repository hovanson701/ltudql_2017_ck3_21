﻿namespace GUI.ManHinhChucNang
{
    partial class ManHinhChucNangLapBaoCaoThongKeDocGiaNoTienPhat
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnWrapperBaoCao = new System.Windows.Forms.Panel();
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnHoanTat = new System.Windows.Forms.Button();
            this.dgvBaoCaoDocGiaNoTienPhat = new System.Windows.Forms.DataGridView();
            this.STT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenDocGia = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TienNo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLamMoi = new System.Windows.Forms.Button();
            this.dtNgayLapBaoCao = new System.Windows.Forms.DateTimePicker();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pnWrapperBaoCao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBaoCaoDocGiaNoTienPhat)).BeginInit();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnWrapperBaoCao
            // 
            this.pnWrapperBaoCao.Controls.Add(this.btnThoat);
            this.pnWrapperBaoCao.Controls.Add(this.btnHoanTat);
            this.pnWrapperBaoCao.Controls.Add(this.dgvBaoCaoDocGiaNoTienPhat);
            this.pnWrapperBaoCao.Controls.Add(this.label3);
            this.pnWrapperBaoCao.Controls.Add(this.btnLamMoi);
            this.pnWrapperBaoCao.Controls.Add(this.dtNgayLapBaoCao);
            this.pnWrapperBaoCao.Controls.Add(this.panel2);
            this.pnWrapperBaoCao.Location = new System.Drawing.Point(0, 0);
            this.pnWrapperBaoCao.Name = "pnWrapperBaoCao";
            this.pnWrapperBaoCao.Size = new System.Drawing.Size(680, 530);
            this.pnWrapperBaoCao.TabIndex = 0;
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(476, 490);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(75, 37);
            this.btnThoat.TabIndex = 55;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnHoanTat
            // 
            this.btnHoanTat.Location = new System.Drawing.Point(302, 490);
            this.btnHoanTat.Name = "btnHoanTat";
            this.btnHoanTat.Size = new System.Drawing.Size(75, 37);
            this.btnHoanTat.TabIndex = 54;
            this.btnHoanTat.Text = "Hoàn tất";
            this.btnHoanTat.UseVisualStyleBackColor = true;
            this.btnHoanTat.Click += new System.EventHandler(this.btnHoanTat_Click);
            // 
            // dgvBaoCaoDocGiaNoTienPhat
            // 
            this.dgvBaoCaoDocGiaNoTienPhat.AllowUserToResizeColumns = false;
            this.dgvBaoCaoDocGiaNoTienPhat.AllowUserToResizeRows = false;
            this.dgvBaoCaoDocGiaNoTienPhat.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBaoCaoDocGiaNoTienPhat.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgvBaoCaoDocGiaNoTienPhat.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBaoCaoDocGiaNoTienPhat.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.STT,
            this.TenDocGia,
            this.TienNo});
            this.dgvBaoCaoDocGiaNoTienPhat.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dgvBaoCaoDocGiaNoTienPhat.Location = new System.Drawing.Point(20, 141);
            this.dgvBaoCaoDocGiaNoTienPhat.MultiSelect = false;
            this.dgvBaoCaoDocGiaNoTienPhat.Name = "dgvBaoCaoDocGiaNoTienPhat";
            this.dgvBaoCaoDocGiaNoTienPhat.RowHeadersVisible = false;
            this.dgvBaoCaoDocGiaNoTienPhat.Size = new System.Drawing.Size(640, 343);
            this.dgvBaoCaoDocGiaNoTienPhat.TabIndex = 53;
            // 
            // STT
            // 
            dataGridViewCellStyle10.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.STT.DefaultCellStyle = dataGridViewCellStyle10;
            this.STT.FillWeight = 50.76143F;
            this.STT.HeaderText = "STT";
            this.STT.Name = "STT";
            this.STT.ReadOnly = true;
            // 
            // TenDocGia
            // 
            this.TenDocGia.DataPropertyName = "HoTenDG";
            dataGridViewCellStyle11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TenDocGia.DefaultCellStyle = dataGridViewCellStyle11;
            this.TenDocGia.FillWeight = 112.3096F;
            this.TenDocGia.HeaderText = "Tên đọc giả";
            this.TenDocGia.Name = "TenDocGia";
            this.TenDocGia.ReadOnly = true;
            // 
            // TienNo
            // 
            this.TienNo.DataPropertyName = "ConLai";
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TienNo.DefaultCellStyle = dataGridViewCellStyle12;
            this.TienNo.FillWeight = 112.3096F;
            this.TienNo.HeaderText = "Tiền nợ";
            this.TienNo.Name = "TienNo";
            this.TienNo.ReadOnly = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(160, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 22);
            this.label3.TabIndex = 52;
            this.label3.Text = "Ngày lập báo cáo";
            // 
            // btnLamMoi
            // 
            this.btnLamMoi.Location = new System.Drawing.Point(131, 490);
            this.btnLamMoi.Name = "btnLamMoi";
            this.btnLamMoi.Size = new System.Drawing.Size(75, 37);
            this.btnLamMoi.TabIndex = 51;
            this.btnLamMoi.Text = "Làm mới";
            this.btnLamMoi.UseVisualStyleBackColor = true;
            this.btnLamMoi.Click += new System.EventHandler(this.btnLamMoi_Click);
            // 
            // dtNgayLapBaoCao
            // 
            this.dtNgayLapBaoCao.CustomFormat = "dd/MM/yyyy";
            this.dtNgayLapBaoCao.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgayLapBaoCao.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayLapBaoCao.Location = new System.Drawing.Point(316, 80);
            this.dtNgayLapBaoCao.Name = "dtNgayLapBaoCao";
            this.dtNgayLapBaoCao.Size = new System.Drawing.Size(200, 32);
            this.dtNgayLapBaoCao.TabIndex = 57;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.SystemColors.Control;
            this.panel2.Controls.Add(this.label1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(680, 60);
            this.panel2.TabIndex = 56;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.label1.Location = new System.Drawing.Point(136, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(415, 26);
            this.label1.TabIndex = 35;
            this.label1.Text = "Lập báo cáo thống kê đọc giả nợ tiền phạt";
            // 
            // ManHinhChucNangLapBaoCaoThongKeDocGiaNoTienPhat
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnWrapperBaoCao);
            this.Name = "ManHinhChucNangLapBaoCaoThongKeDocGiaNoTienPhat";
            this.Size = new System.Drawing.Size(680, 530);
            this.Load += new System.EventHandler(this.ManHinhChucNangLapBaoCaoThongKeDocGiaNoTienPhat_Load);
            this.SizeChanged += new System.EventHandler(this.ManHinhChucNangLapBaoCaoThongKeDocGiaNoTienPhat_SizeChanged);
            this.pnWrapperBaoCao.ResumeLayout(false);
            this.pnWrapperBaoCao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBaoCaoDocGiaNoTienPhat)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnWrapperBaoCao;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Button btnHoanTat;
        private System.Windows.Forms.DataGridView dgvBaoCaoDocGiaNoTienPhat;
        private System.Windows.Forms.DataGridViewTextBoxColumn STT;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenDocGia;
        private System.Windows.Forms.DataGridViewTextBoxColumn TienNo;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLamMoi;
        private System.Windows.Forms.DateTimePicker dtNgayLapBaoCao;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label1;
    }
}
