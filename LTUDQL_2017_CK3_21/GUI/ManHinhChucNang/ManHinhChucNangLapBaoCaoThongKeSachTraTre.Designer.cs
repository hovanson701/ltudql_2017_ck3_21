﻿namespace GUI.ManHinhChucNang
{
    partial class ManHinhChucNangLapBaoCaoThongKeSachTraTre
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            this.pnWrapperBaoCao = new System.Windows.Forms.Panel();
            this.btnThoat = new System.Windows.Forms.Button();
            this.btnHoanTat = new System.Windows.Forms.Button();
            this.dgvBaoCaoSachTraTre = new System.Windows.Forms.DataGridView();
            this.STT = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.TenSach = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NgayMuon = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.SoNgayTraTre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dtNgayLapBaoCao = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.btnLamMoi = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pnWrapperBaoCao.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBaoCaoSachTraTre)).BeginInit();
            this.SuspendLayout();
            // 
            // pnWrapperBaoCao
            // 
            this.pnWrapperBaoCao.Controls.Add(this.btnThoat);
            this.pnWrapperBaoCao.Controls.Add(this.btnHoanTat);
            this.pnWrapperBaoCao.Controls.Add(this.dgvBaoCaoSachTraTre);
            this.pnWrapperBaoCao.Controls.Add(this.dtNgayLapBaoCao);
            this.pnWrapperBaoCao.Controls.Add(this.label3);
            this.pnWrapperBaoCao.Controls.Add(this.btnLamMoi);
            this.pnWrapperBaoCao.Controls.Add(this.label1);
            this.pnWrapperBaoCao.Location = new System.Drawing.Point(0, 0);
            this.pnWrapperBaoCao.Name = "pnWrapperBaoCao";
            this.pnWrapperBaoCao.Size = new System.Drawing.Size(680, 530);
            this.pnWrapperBaoCao.TabIndex = 0;
            // 
            // btnThoat
            // 
            this.btnThoat.Location = new System.Drawing.Point(476, 481);
            this.btnThoat.Name = "btnThoat";
            this.btnThoat.Size = new System.Drawing.Size(75, 37);
            this.btnThoat.TabIndex = 55;
            this.btnThoat.Text = "Thoát";
            this.btnThoat.UseVisualStyleBackColor = true;
            this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
            // 
            // btnHoanTat
            // 
            this.btnHoanTat.Location = new System.Drawing.Point(302, 481);
            this.btnHoanTat.Name = "btnHoanTat";
            this.btnHoanTat.Size = new System.Drawing.Size(75, 37);
            this.btnHoanTat.TabIndex = 54;
            this.btnHoanTat.Text = "Hoàn tất";
            this.btnHoanTat.UseVisualStyleBackColor = true;
            this.btnHoanTat.Click += new System.EventHandler(this.btnHoanTat_Click);
            // 
            // dgvBaoCaoSachTraTre
            // 
            this.dgvBaoCaoSachTraTre.AllowUserToResizeColumns = false;
            this.dgvBaoCaoSachTraTre.AllowUserToResizeRows = false;
            this.dgvBaoCaoSachTraTre.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBaoCaoSachTraTre.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgvBaoCaoSachTraTre.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBaoCaoSachTraTre.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.STT,
            this.TenSach,
            this.NgayMuon,
            this.SoNgayTraTre});
            this.dgvBaoCaoSachTraTre.GridColor = System.Drawing.SystemColors.ControlDarkDark;
            this.dgvBaoCaoSachTraTre.Location = new System.Drawing.Point(20, 132);
            this.dgvBaoCaoSachTraTre.MultiSelect = false;
            this.dgvBaoCaoSachTraTre.Name = "dgvBaoCaoSachTraTre";
            this.dgvBaoCaoSachTraTre.RowHeadersVisible = false;
            this.dgvBaoCaoSachTraTre.Size = new System.Drawing.Size(640, 343);
            this.dgvBaoCaoSachTraTre.TabIndex = 53;
            // 
            // STT
            // 
            dataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.STT.DefaultCellStyle = dataGridViewCellStyle12;
            this.STT.FillWeight = 50.76143F;
            this.STT.HeaderText = "STT";
            this.STT.Name = "STT";
            this.STT.ReadOnly = true;
            // 
            // TenSach
            // 
            this.TenSach.DataPropertyName = "TenSach";
            dataGridViewCellStyle13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.TenSach.DefaultCellStyle = dataGridViewCellStyle13;
            this.TenSach.FillWeight = 112.3096F;
            this.TenSach.HeaderText = "Tên sách";
            this.TenSach.Name = "TenSach";
            this.TenSach.ReadOnly = true;
            // 
            // NgayMuon
            // 
            this.NgayMuon.DataPropertyName = "NgayMuon";
            dataGridViewCellStyle14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.NgayMuon.DefaultCellStyle = dataGridViewCellStyle14;
            this.NgayMuon.FillWeight = 112.3096F;
            this.NgayMuon.HeaderText = "Ngày mượn";
            this.NgayMuon.Name = "NgayMuon";
            this.NgayMuon.ReadOnly = true;
            // 
            // SoNgayTraTre
            // 
            this.SoNgayTraTre.DataPropertyName = "SoNgayTraTre";
            dataGridViewCellStyle15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.SoNgayTraTre.DefaultCellStyle = dataGridViewCellStyle15;
            this.SoNgayTraTre.FillWeight = 112.3096F;
            this.SoNgayTraTre.HeaderText = "Số ngày trả trễ";
            this.SoNgayTraTre.Name = "SoNgayTraTre";
            this.SoNgayTraTre.ReadOnly = true;
            // 
            // dtNgayLapBaoCao
            // 
            this.dtNgayLapBaoCao.CustomFormat = "dd-MM-yyyy";
            this.dtNgayLapBaoCao.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dtNgayLapBaoCao.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtNgayLapBaoCao.Location = new System.Drawing.Point(325, 71);
            this.dtNgayLapBaoCao.Name = "dtNgayLapBaoCao";
            this.dtNgayLapBaoCao.Size = new System.Drawing.Size(200, 32);
            this.dtNgayLapBaoCao.TabIndex = 56;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(152, 81);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(150, 22);
            this.label3.TabIndex = 52;
            this.label3.Text = "Ngày lập báo cáo";
            // 
            // btnLamMoi
            // 
            this.btnLamMoi.Location = new System.Drawing.Point(131, 481);
            this.btnLamMoi.Name = "btnLamMoi";
            this.btnLamMoi.Size = new System.Drawing.Size(75, 37);
            this.btnLamMoi.TabIndex = 51;
            this.btnLamMoi.Text = "Làm mới";
            this.btnLamMoi.UseVisualStyleBackColor = true;
            this.btnLamMoi.Click += new System.EventHandler(this.btnLamMoi_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(173, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(334, 26);
            this.label1.TabIndex = 50;
            this.label1.Text = "Lập báo cáo thống kê sách trả trễ";
            // 
            // ManHinhChucNangLapBaoCaoThongKeSachTraTre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.pnWrapperBaoCao);
            this.Name = "ManHinhChucNangLapBaoCaoThongKeSachTraTre";
            this.Size = new System.Drawing.Size(680, 530);
            this.Load += new System.EventHandler(this.ManHinhChucNangLapBaoCaoThongKeSachTraTre_Load);
            this.SizeChanged += new System.EventHandler(this.ManHinhChucNangLapBaoCaoThongKeSachTraTre_SizeChanged);
            this.pnWrapperBaoCao.ResumeLayout(false);
            this.pnWrapperBaoCao.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBaoCaoSachTraTre)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnWrapperBaoCao;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Button btnHoanTat;
        private System.Windows.Forms.DataGridView dgvBaoCaoSachTraTre;
        private System.Windows.Forms.DataGridViewTextBoxColumn STT;
        private System.Windows.Forms.DataGridViewTextBoxColumn TenSach;
        private System.Windows.Forms.DataGridViewTextBoxColumn NgayMuon;
        private System.Windows.Forms.DataGridViewTextBoxColumn SoNgayTraTre;
        private System.Windows.Forms.DateTimePicker dtNgayLapBaoCao;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnLamMoi;
        private System.Windows.Forms.Label label1;
    }
}
