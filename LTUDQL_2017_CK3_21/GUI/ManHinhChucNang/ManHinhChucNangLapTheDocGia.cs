﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using BUS.DocGiaBUS;
using DTO;
using static GUI.ManHinhThucDonForm;

namespace GUI.ManHinhChucNang
{
    public partial class ManHinhChucNangLapTheDocGia : UserControl
    {
        DocGiaDTO docGia = new DocGiaDTO();
        HamChuyenTrang ChuyenTrangChu;
        public ManHinhChucNangLapTheDocGia(HamChuyenTrang ChuyenTrangChu)
        {
            InitializeComponent();
            this.ChuyenTrangChu = ChuyenTrangChu;
        }
        private void Tai_Thong_Tin() //Load
        {
            //Set format tháng ngày năm
            dtpNgaySinh.Format = DateTimePickerFormat.Custom;
            dtpNgaySinh.CustomFormat = "dd MMMM yyyy";
            dtpNgayTiepNhan.Format = DateTimePickerFormat.Custom;
            dtpNgayTiepNhan.CustomFormat = "dd MMMM yyyy";


            //Tải danh sách Loại độc giả
            LoaiDocGiaBUS loaiDocGiaBUS = new LoaiDocGiaBUS();
            DataTable dsLoaiDocGia = loaiDocGiaBUS.LayDanhSachLoaiDocGia();
            cbbLoaiDocGia.DataSource = dsLoaiDocGia;
            cbbLoaiDocGia.DisplayMember = dsLoaiDocGia.Columns[1].ColumnName;
            cbbLoaiDocGia.ValueMember = dsLoaiDocGia.Columns[0].ColumnName;


            //Tải danh sách nhân viên thủ thư
            NhanVienBUS nhanVienBUS = new NhanVienBUS();
            DataTable dsNhanVien = nhanVienBUS.LayDanhSachNhanVienTheoBoPhan("BP02");
            cbbMaNhanVien.DataSource = dsNhanVien;
            cbbMaNhanVien.DisplayMember = dsNhanVien.Columns[1].ColumnName;
            cbbMaNhanVien.ValueMember = dsNhanVien.Columns[0].ColumnName;

            //Tạo mã cho độc giả mới
            DocGiaBUS docGiaBUS = new DocGiaBUS();
            txtMaDocGia.Text = docGiaBUS.LayMaDocGiaMoi();

        }

        private void LamMoi()
        {
            txtHoTen.Text = "";
            dtpNgaySinh.Value = DateTime.Now;
            txtEmail.Text = "";
            txtDiaChi.Text = "";
        }
        private void ManHinhChucNangLapTheDocGia_Load(object sender, EventArgs e)
        {
            Tai_Thong_Tin();
        }

        private void btnLamMoi_Click(object sender, EventArgs e)
        {
            LamMoi();
        }

        private void btnHoanTat_Click(object sender, EventArgs e)
        {

            try
            {
                docGia.MaDocGia = txtMaDocGia.Text;
                docGia.HoTenDG = txtHoTen.Text;
                docGia.NgaySinh = dtpNgaySinh.Value.ToString("MM/dd/yyyy").Substring(0, 10);
                docGia.DiaChi = txtDiaChi.Text;
                docGia.Email = txtEmail.Text;
                docGia.TTDG = "TTDG01";
                docGia.MaLoaiDG = cbbLoaiDocGia.SelectedValue.ToString();
                docGia.NVLapThe = cbbMaNhanVien.SelectedValue.ToString();
                docGia.NgayLapThe = dtpNgayTiepNhan.Value.ToString("MM/dd/yyyy").Substring(0, 10);
                docGia.TongNo = 0;
                QuyDinhBUS quyDinhBUS = new QuyDinhBUS();
                DataTable dsQuyDinh = quyDinhBUS.LayDanhSachQuyDinh();
                int thoiHanThe = int.Parse(dsQuyDinh.Rows[0][2].ToString());
                docGia.NgayHetHan = dtpNgayTiepNhan.Value.AddMonths(thoiHanThe).ToString("MM/dd/yyyy").Substring(0, 10);

                DocGiaBUS docGiaBUS = new DocGiaBUS();
                docGiaBUS.ThemDocGia(docGia);
                MessageBox.Show("Thêm thành công");

                LamMoi();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Thêm thất bại. Lỗi:" + ex.ToString());
            }
        }

        private void btnChonHinh_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Chức năng đang trong giai đoạn bảo trì");
        }

        private void btnChupHinh_Click(object sender, EventArgs e)
        {

            MessageBox.Show("Chức năng đang trong giai đoạn bảo trì");
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChu(0);
        }
    }
}
