﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using BUS;
using static GUI.ManHinhThucDonForm;

namespace GUI.ManHinhChucNang
{
    public partial class ManHinhChucNangQuanLyNhanVien : UserControl
    {
        DataTable dsNhanVien;
        int LanDauDoDuLieu = 0;
        HamChuyenTrang ChuyenTrangChu;
        public ManHinhChucNangQuanLyNhanVien(HamChuyenTrang ChuyenTrangChu)
        {
            InitializeComponent();
            this.ChuyenTrangChu = ChuyenTrangChu;
        }
        void Tai_Thong_Tin() //Load dữ liệu nhân viên 
        {
            NhanVienBUS nhanVienBUS = new NhanVienBUS();
            dsNhanVien = nhanVienBUS.LayDanhSachNhanVien();

            if (LanDauDoDuLieu == 0)
            {

                //Tải danh sách tình trạng nhân viên

                TinhTrangNhanVienBUS tinhTrangNhanVienBUS = new TinhTrangNhanVienBUS();
                DataTable dsTTNV = tinhTrangNhanVienBUS.LayDanhSachTinhTrangNhanVien();

                DataGridViewComboBoxColumn dgvComboTinhTrangNhanVien = new DataGridViewComboBoxColumn();
                dgvComboTinhTrangNhanVien.DataSource = dsTTNV;
                dgvComboTinhTrangNhanVien.DisplayMember = dsTTNV.Columns[1].ColumnName;
                dgvComboTinhTrangNhanVien.ValueMember = dsTTNV.Columns[0].ColumnName;
                dgvComboTinhTrangNhanVien.DataPropertyName = "MaTTNV";
                dgvComboTinhTrangNhanVien.HeaderText = "Tình Trạng";
                dgvComboTinhTrangNhanVien.Width = 50;

                dgvDanhSachNhanVien.Columns.Add(dgvComboTinhTrangNhanVien);

                //Tải danh sách bằng cấp
                BangCapBUS bangCapBUS = new BangCapBUS();
                DataTable dsBangCap = bangCapBUS.LayDanhSachBangCap();

                //Thêm cột combobox Bằng cấp vào datagridview
                DataGridViewComboBoxColumn dgvComboBangCap = new DataGridViewComboBoxColumn();
                dgvComboBangCap.DataSource = dsBangCap;
                dgvComboBangCap.DisplayMember = dsBangCap.Columns[1].ColumnName;
                dgvComboBangCap.ValueMember = dsBangCap.Columns[0].ColumnName;
                dgvComboBangCap.DataPropertyName = "MaBangCap";
                dgvComboBangCap.HeaderText = "Bằng cấp";
                dgvComboBangCap.Width = 50;

                dgvDanhSachNhanVien.Columns.Add(dgvComboBangCap);


                //Tải danh sách bộ phận
                BoPhanBUS boPhanBUS = new BoPhanBUS();
                DataTable dsBoPhan = boPhanBUS.LayDanhSachBoPhan();

                //Thêm cột combobox Bộ phận vào datagridview
                DataGridViewComboBoxColumn dgvComboBoPhan = new DataGridViewComboBoxColumn();
                dgvComboBoPhan.DataSource = dsBoPhan;
                dgvComboBoPhan.DisplayMember = dsBoPhan.Columns[1].ColumnName;
                dgvComboBoPhan.ValueMember = dsBoPhan.Columns[0].ColumnName;
                dgvComboBoPhan.DataPropertyName = "MaBoPhan";
                dgvComboBoPhan.HeaderText = "Bộ phận";
                dgvComboBoPhan.Width = 50;

                dgvDanhSachNhanVien.Columns.Add(dgvComboBoPhan);
                //Tải danh sách chức vụ
                ChucVuBUS chucVuBUS = new ChucVuBUS();
                DataTable dsChucVu = chucVuBUS.LayDanhSachChucVu();

                //Thêm cột combobox Chức vụ vào datagridview
                DataGridViewComboBoxColumn dgvComboChucVu = new DataGridViewComboBoxColumn();
                dgvComboChucVu.DataSource = dsChucVu;
                dgvComboChucVu.DisplayMember = dsChucVu.Columns[1].ColumnName;
                dgvComboChucVu.ValueMember = dsChucVu.Columns[0].ColumnName;
                dgvComboChucVu.DataPropertyName = "MaChucVu";
                dgvComboChucVu.HeaderText = "Chức vụ";
                dgvComboChucVu.Width = 50;
                dgvDanhSachNhanVien.Columns.Add(dgvComboChucVu);

                LanDauDoDuLieu++;
            }
            //Đổ data vào datagridview
            dgvDanhSachNhanVien.DataSource = dsNhanVien;

        }

        

        private void ManHinhChucNangQuanLyNhanVien_Load(object sender, EventArgs e)
        {
            Tai_Thong_Tin();
        }

        private void btnCapNhat_Click(object sender, EventArgs e)
        {
            dgvDanhSachNhanVien.Update();
            DataTable dataTableUpdate = new DataTable();
            dataTableUpdate = dsNhanVien.GetChanges();
            if (dataTableUpdate != null)
            {
                NhanVienBUS nhanVienBUS = new NhanVienBUS();
                nhanVienBUS.CapNhatNhanVien(dataTableUpdate);
                Tai_Thong_Tin();
            }
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {

            this.ChuyenTrangChu(0);
        }
    }
}
