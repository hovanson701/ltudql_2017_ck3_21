﻿namespace GUI
{
    partial class ManHinhThucDonForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnHeader = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lbTenPhanMem = new System.Windows.Forms.Label();
            this.pnWrapper = new System.Windows.Forms.Panel();
            this.pnContent = new System.Windows.Forms.Panel();
            this.pnRight = new System.Windows.Forms.Panel();
            this.pnMainMenu = new System.Windows.Forms.Panel();
            this.pnSubMenu = new System.Windows.Forms.Panel();
            this.pnChucNang = new System.Windows.Forms.Panel();
            this.pnTieuDeChucNang = new System.Windows.Forms.Panel();
            this.lbChucNang = new System.Windows.Forms.Label();
            this.pnMenu = new System.Windows.Forms.Panel();
            this.pnWrapperMenu = new System.Windows.Forms.Panel();
            this.btnItemQuyDinh = new System.Windows.Forms.Button();
            this.btnItemLuuTru = new System.Windows.Forms.Button();
            this.btnItemImportExport = new System.Windows.Forms.Button();
            this.btnItemThuVien = new System.Windows.Forms.Button();
            this.btnItemDocGia = new System.Windows.Forms.Button();
            this.btnItemSach = new System.Windows.Forms.Button();
            this.btnItemHeThong = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnTrangChu = new System.Windows.Forms.Button();
            this.pnHeader.SuspendLayout();
            this.pnWrapper.SuspendLayout();
            this.pnMainMenu.SuspendLayout();
            this.pnSubMenu.SuspendLayout();
            this.pnTieuDeChucNang.SuspendLayout();
            this.pnMenu.SuspendLayout();
            this.pnWrapperMenu.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnHeader
            // 
            this.pnHeader.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(216)))));
            this.pnHeader.Controls.Add(this.label1);
            this.pnHeader.Controls.Add(this.panel2);
            this.pnHeader.Controls.Add(this.lbTenPhanMem);
            this.pnHeader.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnHeader.Location = new System.Drawing.Point(0, 0);
            this.pnHeader.Name = "pnHeader";
            this.pnHeader.Size = new System.Drawing.Size(1180, 61);
            this.pnHeader.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(749, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(419, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Lập trình ứng dụng quản lý 1, Nhóm 21, 17CK3";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 60);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1180, 1);
            this.panel2.TabIndex = 0;
            // 
            // lbTenPhanMem
            // 
            this.lbTenPhanMem.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbTenPhanMem.AutoSize = true;
            this.lbTenPhanMem.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbTenPhanMem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbTenPhanMem.Location = new System.Drawing.Point(57, 11);
            this.lbTenPhanMem.Name = "lbTenPhanMem";
            this.lbTenPhanMem.Size = new System.Drawing.Size(306, 46);
            this.lbTenPhanMem.TabIndex = 0;
            this.lbTenPhanMem.Text = "Quản lý thư viện";
            // 
            // pnWrapper
            // 
            this.pnWrapper.BackColor = System.Drawing.Color.White;
            this.pnWrapper.Controls.Add(this.pnContent);
            this.pnWrapper.Controls.Add(this.pnRight);
            this.pnWrapper.Controls.Add(this.pnMainMenu);
            this.pnWrapper.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWrapper.Location = new System.Drawing.Point(0, 61);
            this.pnWrapper.Name = "pnWrapper";
            this.pnWrapper.Size = new System.Drawing.Size(1180, 530);
            this.pnWrapper.TabIndex = 2;
            // 
            // pnContent
            // 
            this.pnContent.BackColor = System.Drawing.SystemColors.Control;
            this.pnContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnContent.Location = new System.Drawing.Point(250, 0);
            this.pnContent.Name = "pnContent";
            this.pnContent.Size = new System.Drawing.Size(680, 530);
            this.pnContent.TabIndex = 2;
            // 
            // pnRight
            // 
            this.pnRight.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(250)))));
            this.pnRight.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnRight.Location = new System.Drawing.Point(930, 0);
            this.pnRight.Name = "pnRight";
            this.pnRight.Size = new System.Drawing.Size(250, 530);
            this.pnRight.TabIndex = 1;
            // 
            // pnMainMenu
            // 
            this.pnMainMenu.BackColor = System.Drawing.Color.White;
            this.pnMainMenu.Controls.Add(this.pnSubMenu);
            this.pnMainMenu.Controls.Add(this.pnMenu);
            this.pnMainMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnMainMenu.ForeColor = System.Drawing.SystemColors.ControlText;
            this.pnMainMenu.Location = new System.Drawing.Point(0, 0);
            this.pnMainMenu.Name = "pnMainMenu";
            this.pnMainMenu.Size = new System.Drawing.Size(250, 530);
            this.pnMainMenu.TabIndex = 0;
            // 
            // pnSubMenu
            // 
            this.pnSubMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(250)))));
            this.pnSubMenu.Controls.Add(this.pnChucNang);
            this.pnSubMenu.Controls.Add(this.pnTieuDeChucNang);
            this.pnSubMenu.Dock = System.Windows.Forms.DockStyle.Right;
            this.pnSubMenu.Location = new System.Drawing.Point(65, 0);
            this.pnSubMenu.Name = "pnSubMenu";
            this.pnSubMenu.Size = new System.Drawing.Size(185, 530);
            this.pnSubMenu.TabIndex = 0;
            // 
            // pnChucNang
            // 
            this.pnChucNang.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnChucNang.Location = new System.Drawing.Point(0, 60);
            this.pnChucNang.Name = "pnChucNang";
            this.pnChucNang.Size = new System.Drawing.Size(185, 470);
            this.pnChucNang.TabIndex = 0;
            // 
            // pnTieuDeChucNang
            // 
            this.pnTieuDeChucNang.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(112)))), ((int)(((byte)(201)))));
            this.pnTieuDeChucNang.Controls.Add(this.lbChucNang);
            this.pnTieuDeChucNang.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTieuDeChucNang.Location = new System.Drawing.Point(0, 0);
            this.pnTieuDeChucNang.Name = "pnTieuDeChucNang";
            this.pnTieuDeChucNang.Size = new System.Drawing.Size(185, 60);
            this.pnTieuDeChucNang.TabIndex = 0;
            // 
            // lbChucNang
            // 
            this.lbChucNang.AutoSize = true;
            this.lbChucNang.Font = new System.Drawing.Font("Microsoft Sans Serif", 20F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbChucNang.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lbChucNang.Location = new System.Drawing.Point(16, 14);
            this.lbChucNang.Name = "lbChucNang";
            this.lbChucNang.Size = new System.Drawing.Size(154, 31);
            this.lbChucNang.TabIndex = 0;
            this.lbChucNang.Text = "Chức năng";
            // 
            // pnMenu
            // 
            this.pnMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(216)))));
            this.pnMenu.Controls.Add(this.pnWrapperMenu);
            this.pnMenu.Controls.Add(this.panel1);
            this.pnMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.pnMenu.Location = new System.Drawing.Point(0, 0);
            this.pnMenu.Name = "pnMenu";
            this.pnMenu.Size = new System.Drawing.Size(70, 530);
            this.pnMenu.TabIndex = 0;
            // 
            // pnWrapperMenu
            // 
            this.pnWrapperMenu.Controls.Add(this.btnItemQuyDinh);
            this.pnWrapperMenu.Controls.Add(this.btnItemLuuTru);
            this.pnWrapperMenu.Controls.Add(this.btnItemImportExport);
            this.pnWrapperMenu.Controls.Add(this.btnItemThuVien);
            this.pnWrapperMenu.Controls.Add(this.btnItemDocGia);
            this.pnWrapperMenu.Controls.Add(this.btnItemSach);
            this.pnWrapperMenu.Controls.Add(this.btnItemHeThong);
            this.pnWrapperMenu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnWrapperMenu.Location = new System.Drawing.Point(0, 60);
            this.pnWrapperMenu.Name = "pnWrapperMenu";
            this.pnWrapperMenu.Size = new System.Drawing.Size(70, 470);
            this.pnWrapperMenu.TabIndex = 0;
            // 
            // btnItemQuyDinh
            // 
            this.btnItemQuyDinh.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemQuyDinh.FlatAppearance.BorderSize = 0;
            this.btnItemQuyDinh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemQuyDinh.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemQuyDinh.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemQuyDinh.Image = global::GUI.Properties.Resources.iconQuyDinhTrang;
            this.btnItemQuyDinh.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnItemQuyDinh.Location = new System.Drawing.Point(0, 360);
            this.btnItemQuyDinh.Name = "btnItemQuyDinh";
            this.btnItemQuyDinh.Size = new System.Drawing.Size(70, 60);
            this.btnItemQuyDinh.TabIndex = 6;
            this.btnItemQuyDinh.Text = "Quy định";
            this.btnItemQuyDinh.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnItemQuyDinh.UseVisualStyleBackColor = true;
            this.btnItemQuyDinh.Click += new System.EventHandler(this.btnItemQuyDinh_Click);
            // 
            // btnItemLuuTru
            // 
            this.btnItemLuuTru.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemLuuTru.FlatAppearance.BorderSize = 0;
            this.btnItemLuuTru.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemLuuTru.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemLuuTru.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemLuuTru.Image = global::GUI.Properties.Resources.iconLuuTruTrang;
            this.btnItemLuuTru.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnItemLuuTru.Location = new System.Drawing.Point(0, 300);
            this.btnItemLuuTru.Name = "btnItemLuuTru";
            this.btnItemLuuTru.Size = new System.Drawing.Size(70, 60);
            this.btnItemLuuTru.TabIndex = 5;
            this.btnItemLuuTru.Text = "Lưu trữ";
            this.btnItemLuuTru.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnItemLuuTru.UseVisualStyleBackColor = true;
            this.btnItemLuuTru.Click += new System.EventHandler(this.btnItemLuuTru_Click);
            // 
            // btnItemImportExport
            // 
            this.btnItemImportExport.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemImportExport.FlatAppearance.BorderSize = 0;
            this.btnItemImportExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemImportExport.Font = new System.Drawing.Font("Tahoma", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemImportExport.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemImportExport.Image = global::GUI.Properties.Resources.iconEx_ImportTrang;
            this.btnItemImportExport.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnItemImportExport.Location = new System.Drawing.Point(0, 240);
            this.btnItemImportExport.Margin = new System.Windows.Forms.Padding(0);
            this.btnItemImportExport.Name = "btnItemImportExport";
            this.btnItemImportExport.Size = new System.Drawing.Size(70, 60);
            this.btnItemImportExport.TabIndex = 4;
            this.btnItemImportExport.Text = "Ex-Import";
            this.btnItemImportExport.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnItemImportExport.UseVisualStyleBackColor = true;
            this.btnItemImportExport.Click += new System.EventHandler(this.btnItemImportExport_Click);
            // 
            // btnItemThuVien
            // 
            this.btnItemThuVien.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemThuVien.FlatAppearance.BorderSize = 0;
            this.btnItemThuVien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemThuVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemThuVien.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemThuVien.Image = global::GUI.Properties.Resources.iconThuVienTrang;
            this.btnItemThuVien.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnItemThuVien.Location = new System.Drawing.Point(0, 180);
            this.btnItemThuVien.Name = "btnItemThuVien";
            this.btnItemThuVien.Size = new System.Drawing.Size(70, 60);
            this.btnItemThuVien.TabIndex = 3;
            this.btnItemThuVien.Text = "Thư viện";
            this.btnItemThuVien.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnItemThuVien.UseVisualStyleBackColor = true;
            this.btnItemThuVien.Click += new System.EventHandler(this.btnItemThuVien_Click);
            // 
            // btnItemDocGia
            // 
            this.btnItemDocGia.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemDocGia.FlatAppearance.BorderSize = 0;
            this.btnItemDocGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemDocGia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemDocGia.Image = global::GUI.Properties.Resources.iconDocGia;
            this.btnItemDocGia.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnItemDocGia.Location = new System.Drawing.Point(0, 120);
            this.btnItemDocGia.Name = "btnItemDocGia";
            this.btnItemDocGia.Size = new System.Drawing.Size(70, 60);
            this.btnItemDocGia.TabIndex = 2;
            this.btnItemDocGia.Text = "Đọc giả";
            this.btnItemDocGia.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnItemDocGia.UseVisualStyleBackColor = true;
            this.btnItemDocGia.Click += new System.EventHandler(this.btnItemDocGia_Click);
            // 
            // btnItemSach
            // 
            this.btnItemSach.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemSach.FlatAppearance.BorderSize = 0;
            this.btnItemSach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemSach.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemSach.Image = global::GUI.Properties.Resources.iconSachTrang;
            this.btnItemSach.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnItemSach.Location = new System.Drawing.Point(0, 60);
            this.btnItemSach.Name = "btnItemSach";
            this.btnItemSach.Size = new System.Drawing.Size(70, 60);
            this.btnItemSach.TabIndex = 1;
            this.btnItemSach.Text = "Sách";
            this.btnItemSach.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnItemSach.UseVisualStyleBackColor = true;
            this.btnItemSach.Click += new System.EventHandler(this.btnItemSach_Click);
            // 
            // btnItemHeThong
            // 
            this.btnItemHeThong.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(216)))));
            this.btnItemHeThong.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemHeThong.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnItemHeThong.FlatAppearance.BorderSize = 0;
            this.btnItemHeThong.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemHeThong.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemHeThong.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemHeThong.Image = global::GUI.Properties.Resources.iconHeThongTrang;
            this.btnItemHeThong.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnItemHeThong.Location = new System.Drawing.Point(0, 0);
            this.btnItemHeThong.Name = "btnItemHeThong";
            this.btnItemHeThong.Size = new System.Drawing.Size(70, 60);
            this.btnItemHeThong.TabIndex = 0;
            this.btnItemHeThong.Text = "Hệ thống";
            this.btnItemHeThong.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnItemHeThong.UseVisualStyleBackColor = false;
            this.btnItemHeThong.Click += new System.EventHandler(this.btnItemHeThong_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(216)))));
            this.panel1.Controls.Add(this.btnTrangChu);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(70, 60);
            this.panel1.TabIndex = 2;
            // 
            // btnTrangChu
            // 
            this.btnTrangChu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(127)))), ((int)(((byte)(216)))));
            this.btnTrangChu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTrangChu.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.btnTrangChu.FlatAppearance.BorderSize = 0;
            this.btnTrangChu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrangChu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTrangChu.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTrangChu.Image = global::GUI.Properties.Resources.icondasdboad;
            this.btnTrangChu.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnTrangChu.Location = new System.Drawing.Point(0, 0);
            this.btnTrangChu.Name = "btnTrangChu";
            this.btnTrangChu.Size = new System.Drawing.Size(70, 60);
            this.btnTrangChu.TabIndex = 7;
            this.btnTrangChu.Text = "Trang chủ";
            this.btnTrangChu.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTrangChu.UseVisualStyleBackColor = false;
            this.btnTrangChu.Click += new System.EventHandler(this.btnTrangChu_Click);
            // 
            // ManHinhThucDonForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1180, 591);
            this.Controls.Add(this.pnWrapper);
            this.Controls.Add(this.pnHeader);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "ManHinhThucDonForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Màn hình thực đơn";
            this.pnHeader.ResumeLayout(false);
            this.pnHeader.PerformLayout();
            this.pnWrapper.ResumeLayout(false);
            this.pnMainMenu.ResumeLayout(false);
            this.pnSubMenu.ResumeLayout(false);
            this.pnTieuDeChucNang.ResumeLayout(false);
            this.pnTieuDeChucNang.PerformLayout();
            this.pnMenu.ResumeLayout(false);
            this.pnWrapperMenu.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnHeader;
        private System.Windows.Forms.Panel pnWrapper;
        private System.Windows.Forms.Label lbTenPhanMem;
        private System.Windows.Forms.Panel pnRight;
        private System.Windows.Forms.Panel pnMainMenu;
        private System.Windows.Forms.Panel pnMenu;
        private System.Windows.Forms.Panel pnWrapperMenu;
        private System.Windows.Forms.Button btnItemQuyDinh;
        private System.Windows.Forms.Button btnItemLuuTru;
        private System.Windows.Forms.Button btnItemImportExport;
        private System.Windows.Forms.Button btnItemThuVien;
        private System.Windows.Forms.Button btnItemDocGia;
        private System.Windows.Forms.Button btnItemSach;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel pnContent;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnItemHeThong;
        private System.Windows.Forms.Panel pnSubMenu;
        private System.Windows.Forms.Panel pnTieuDeChucNang;
        private System.Windows.Forms.Panel pnChucNang;
        private System.Windows.Forms.Label lbChucNang;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnTrangChu;
    }
}

