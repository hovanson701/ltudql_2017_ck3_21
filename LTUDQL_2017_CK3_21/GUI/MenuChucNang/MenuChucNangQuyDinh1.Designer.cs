﻿namespace GUI.MenuChucNang
{
    partial class MenuChucNangQuyDinh1
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnChucNangQuyDinh1 = new System.Windows.Forms.Panel();
            this.btnItemCapNhatLyDoThanhLy = new System.Windows.Forms.Button();
            this.btnItemCapNhatLoaiDocGia = new System.Windows.Forms.Button();
            this.btnItemCapNhatBangCap = new System.Windows.Forms.Button();
            this.btnItemCapNhatQuyDinh = new System.Windows.Forms.Button();
            this.timerAnimationSlide = new System.Windows.Forms.Timer(this.components);
            this.button1 = new System.Windows.Forms.Button();
            this.pnChucNangQuyDinh1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnChucNangQuyDinh1
            // 
            this.pnChucNangQuyDinh1.Controls.Add(this.button1);
            this.pnChucNangQuyDinh1.Controls.Add(this.btnItemCapNhatLyDoThanhLy);
            this.pnChucNangQuyDinh1.Controls.Add(this.btnItemCapNhatLoaiDocGia);
            this.pnChucNangQuyDinh1.Controls.Add(this.btnItemCapNhatBangCap);
            this.pnChucNangQuyDinh1.Controls.Add(this.btnItemCapNhatQuyDinh);
            this.pnChucNangQuyDinh1.Location = new System.Drawing.Point(0, 0);
            this.pnChucNangQuyDinh1.Name = "pnChucNangQuyDinh1";
            this.pnChucNangQuyDinh1.Size = new System.Drawing.Size(185, 470);
            this.pnChucNangQuyDinh1.TabIndex = 0;
            // 
            // btnItemCapNhatLyDoThanhLy
            // 
            this.btnItemCapNhatLyDoThanhLy.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemCapNhatLyDoThanhLy.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemCapNhatLyDoThanhLy.FlatAppearance.BorderSize = 0;
            this.btnItemCapNhatLyDoThanhLy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemCapNhatLyDoThanhLy.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemCapNhatLyDoThanhLy.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemCapNhatLyDoThanhLy.Location = new System.Drawing.Point(0, 180);
            this.btnItemCapNhatLyDoThanhLy.Name = "btnItemCapNhatLyDoThanhLy";
            this.btnItemCapNhatLyDoThanhLy.Size = new System.Drawing.Size(185, 60);
            this.btnItemCapNhatLyDoThanhLy.TabIndex = 25;
            this.btnItemCapNhatLyDoThanhLy.Text = "Cập nhật lý do thanh lý sách";
            this.btnItemCapNhatLyDoThanhLy.UseVisualStyleBackColor = true;
            this.btnItemCapNhatLyDoThanhLy.Click += new System.EventHandler(this.btnItemCapNhatLyDoThanhLy_Click);
            // 
            // btnItemCapNhatLoaiDocGia
            // 
            this.btnItemCapNhatLoaiDocGia.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemCapNhatLoaiDocGia.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemCapNhatLoaiDocGia.FlatAppearance.BorderSize = 0;
            this.btnItemCapNhatLoaiDocGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemCapNhatLoaiDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemCapNhatLoaiDocGia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemCapNhatLoaiDocGia.Location = new System.Drawing.Point(0, 120);
            this.btnItemCapNhatLoaiDocGia.Name = "btnItemCapNhatLoaiDocGia";
            this.btnItemCapNhatLoaiDocGia.Size = new System.Drawing.Size(185, 60);
            this.btnItemCapNhatLoaiDocGia.TabIndex = 24;
            this.btnItemCapNhatLoaiDocGia.Text = "Cập nhật quy định liên quan đến độc giả";
            this.btnItemCapNhatLoaiDocGia.UseVisualStyleBackColor = true;
            this.btnItemCapNhatLoaiDocGia.Click += new System.EventHandler(this.btnItemCapNhatLoaiDocGia_Click);
            // 
            // btnItemCapNhatBangCap
            // 
            this.btnItemCapNhatBangCap.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemCapNhatBangCap.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemCapNhatBangCap.FlatAppearance.BorderSize = 0;
            this.btnItemCapNhatBangCap.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemCapNhatBangCap.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemCapNhatBangCap.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemCapNhatBangCap.Location = new System.Drawing.Point(0, 60);
            this.btnItemCapNhatBangCap.Name = "btnItemCapNhatBangCap";
            this.btnItemCapNhatBangCap.Size = new System.Drawing.Size(185, 60);
            this.btnItemCapNhatBangCap.TabIndex = 20;
            this.btnItemCapNhatBangCap.Text = "Cập nhật quy định liên quan đến nhân viên";
            this.btnItemCapNhatBangCap.UseVisualStyleBackColor = true;
            this.btnItemCapNhatBangCap.Click += new System.EventHandler(this.btnItemCapNhatBangCap_Click);
            // 
            // btnItemCapNhatQuyDinh
            // 
            this.btnItemCapNhatQuyDinh.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemCapNhatQuyDinh.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemCapNhatQuyDinh.FlatAppearance.BorderSize = 0;
            this.btnItemCapNhatQuyDinh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemCapNhatQuyDinh.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemCapNhatQuyDinh.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemCapNhatQuyDinh.Location = new System.Drawing.Point(0, 0);
            this.btnItemCapNhatQuyDinh.Name = "btnItemCapNhatQuyDinh";
            this.btnItemCapNhatQuyDinh.Size = new System.Drawing.Size(185, 60);
            this.btnItemCapNhatQuyDinh.TabIndex = 19;
            this.btnItemCapNhatQuyDinh.Text = "Cập nhật quy định chung";
            this.btnItemCapNhatQuyDinh.UseVisualStyleBackColor = true;
            this.btnItemCapNhatQuyDinh.Click += new System.EventHandler(this.btnItemCapNhatQuyDinh_Click);
            // 
            // timerAnimationSlide
            // 
            this.timerAnimationSlide.Interval = 1;
            this.timerAnimationSlide.Tick += new System.EventHandler(this.timerAnimationSlide_Tick);
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Top;
            this.button1.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(0, 240);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(185, 60);
            this.button1.TabIndex = 26;
            this.button1.Text = "Cập nhật các quy định liên quan đến bảng sách";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // MenuChucNangQuyDinh1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(250)))));
            this.Controls.Add(this.pnChucNangQuyDinh1);
            this.Name = "MenuChucNangQuyDinh1";
            this.Size = new System.Drawing.Size(185, 470);
            this.Load += new System.EventHandler(this.MenuChucNangQuyDinh1_Load);
            this.pnChucNangQuyDinh1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnChucNangQuyDinh1;
        private System.Windows.Forms.Button btnItemCapNhatLoaiDocGia;
        private System.Windows.Forms.Button btnItemCapNhatBangCap;
        private System.Windows.Forms.Button btnItemCapNhatQuyDinh;
        private System.Windows.Forms.Timer timerAnimationSlide;
        private System.Windows.Forms.Button btnItemCapNhatLyDoThanhLy;
        private System.Windows.Forms.Button button1;
    }
}
