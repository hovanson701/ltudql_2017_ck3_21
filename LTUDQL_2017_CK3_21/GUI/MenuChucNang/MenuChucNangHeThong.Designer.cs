﻿namespace GUI
{
    partial class MenuChucNangHeThong
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerAnimationSlide = new System.Windows.Forms.Timer(this.components);
            this.pnChucNangHeThong = new System.Windows.Forms.Panel();
            this.btnItemCauHinhPhanMem = new System.Windows.Forms.Button();
            this.btnItemCauHinhThietBi = new System.Windows.Forms.Button();
            this.btnItemCapNhatPhanQuyen = new System.Windows.Forms.Button();
            this.btnItemDoiMatKhau = new System.Windows.Forms.Button();
            this.btnItemDangXuat = new System.Windows.Forms.Button();
            this.pnChucNangHeThong.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerAnimationSlide
            // 
            this.timerAnimationSlide.Interval = 5;
            this.timerAnimationSlide.Tick += new System.EventHandler(this.timerAnimationSlide_Tick);
            // 
            // pnChucNangHeThong
            // 
            this.pnChucNangHeThong.Controls.Add(this.btnItemCauHinhPhanMem);
            this.pnChucNangHeThong.Controls.Add(this.btnItemCauHinhThietBi);
            this.pnChucNangHeThong.Controls.Add(this.btnItemCapNhatPhanQuyen);
            this.pnChucNangHeThong.Controls.Add(this.btnItemDoiMatKhau);
            this.pnChucNangHeThong.Controls.Add(this.btnItemDangXuat);
            this.pnChucNangHeThong.Location = new System.Drawing.Point(0, 0);
            this.pnChucNangHeThong.Margin = new System.Windows.Forms.Padding(4);
            this.pnChucNangHeThong.Name = "pnChucNangHeThong";
            this.pnChucNangHeThong.Size = new System.Drawing.Size(247, 578);
            this.pnChucNangHeThong.TabIndex = 6;
            // 
            // btnItemCauHinhPhanMem
            // 
            this.btnItemCauHinhPhanMem.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemCauHinhPhanMem.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemCauHinhPhanMem.FlatAppearance.BorderSize = 0;
            this.btnItemCauHinhPhanMem.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemCauHinhPhanMem.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemCauHinhPhanMem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemCauHinhPhanMem.Location = new System.Drawing.Point(0, 296);
            this.btnItemCauHinhPhanMem.Margin = new System.Windows.Forms.Padding(4);
            this.btnItemCauHinhPhanMem.Name = "btnItemCauHinhPhanMem";
            this.btnItemCauHinhPhanMem.Size = new System.Drawing.Size(247, 74);
            this.btnItemCauHinhPhanMem.TabIndex = 10;
            this.btnItemCauHinhPhanMem.Text = "Cấu hình phần mềm";
            this.btnItemCauHinhPhanMem.UseVisualStyleBackColor = true;
            // 
            // btnItemCauHinhThietBi
            // 
            this.btnItemCauHinhThietBi.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemCauHinhThietBi.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemCauHinhThietBi.FlatAppearance.BorderSize = 0;
            this.btnItemCauHinhThietBi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemCauHinhThietBi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemCauHinhThietBi.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemCauHinhThietBi.Location = new System.Drawing.Point(0, 222);
            this.btnItemCauHinhThietBi.Margin = new System.Windows.Forms.Padding(4);
            this.btnItemCauHinhThietBi.Name = "btnItemCauHinhThietBi";
            this.btnItemCauHinhThietBi.Size = new System.Drawing.Size(247, 74);
            this.btnItemCauHinhThietBi.TabIndex = 9;
            this.btnItemCauHinhThietBi.Text = "Cấu hình thiết bị";
            this.btnItemCauHinhThietBi.UseVisualStyleBackColor = true;
            // 
            // btnItemCapNhatPhanQuyen
            // 
            this.btnItemCapNhatPhanQuyen.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemCapNhatPhanQuyen.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemCapNhatPhanQuyen.FlatAppearance.BorderSize = 0;
            this.btnItemCapNhatPhanQuyen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemCapNhatPhanQuyen.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemCapNhatPhanQuyen.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemCapNhatPhanQuyen.Location = new System.Drawing.Point(0, 148);
            this.btnItemCapNhatPhanQuyen.Margin = new System.Windows.Forms.Padding(4);
            this.btnItemCapNhatPhanQuyen.Name = "btnItemCapNhatPhanQuyen";
            this.btnItemCapNhatPhanQuyen.Size = new System.Drawing.Size(247, 74);
            this.btnItemCapNhatPhanQuyen.TabIndex = 8;
            this.btnItemCapNhatPhanQuyen.Text = "Cập nhật phân quyền";
            this.btnItemCapNhatPhanQuyen.UseVisualStyleBackColor = true;
            // 
            // btnItemDoiMatKhau
            // 
            this.btnItemDoiMatKhau.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemDoiMatKhau.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemDoiMatKhau.FlatAppearance.BorderSize = 0;
            this.btnItemDoiMatKhau.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemDoiMatKhau.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemDoiMatKhau.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemDoiMatKhau.Location = new System.Drawing.Point(0, 74);
            this.btnItemDoiMatKhau.Margin = new System.Windows.Forms.Padding(4);
            this.btnItemDoiMatKhau.Name = "btnItemDoiMatKhau";
            this.btnItemDoiMatKhau.Size = new System.Drawing.Size(247, 74);
            this.btnItemDoiMatKhau.TabIndex = 7;
            this.btnItemDoiMatKhau.Text = "Đổi mật khẩu";
            this.btnItemDoiMatKhau.UseVisualStyleBackColor = true;
            // 
            // btnItemDangXuat
            // 
            this.btnItemDangXuat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemDangXuat.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemDangXuat.FlatAppearance.BorderSize = 0;
            this.btnItemDangXuat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemDangXuat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemDangXuat.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemDangXuat.Location = new System.Drawing.Point(0, 0);
            this.btnItemDangXuat.Margin = new System.Windows.Forms.Padding(4);
            this.btnItemDangXuat.Name = "btnItemDangXuat";
            this.btnItemDangXuat.Size = new System.Drawing.Size(247, 74);
            this.btnItemDangXuat.TabIndex = 6;
            this.btnItemDangXuat.Text = "Đăng xuất";
            this.btnItemDangXuat.UseVisualStyleBackColor = true;
            // 
            // MenuChucNangHeThong
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(250)))));
            this.Controls.Add(this.pnChucNangHeThong);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "MenuChucNangHeThong";
            this.Size = new System.Drawing.Size(247, 578);
            this.Load += new System.EventHandler(this.MenuChucNangHeThong_Load);
            this.pnChucNangHeThong.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timerAnimationSlide;
        private System.Windows.Forms.Panel pnChucNangHeThong;
        private System.Windows.Forms.Button btnItemCauHinhPhanMem;
        private System.Windows.Forms.Button btnItemCauHinhThietBi;
        private System.Windows.Forms.Button btnItemCapNhatPhanQuyen;
        private System.Windows.Forms.Button btnItemDoiMatKhau;
        private System.Windows.Forms.Button btnItemDangXuat;
    }
}
