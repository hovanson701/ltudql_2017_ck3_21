﻿namespace GUI.MenuChucNang
{
    partial class MenuChucNangThuVien
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerAnimationSlide = new System.Windows.Forms.Timer(this.components);
            this.pnChucNangThuVien = new System.Windows.Forms.Panel();
            this.btnItemQuanLyNhanVien = new System.Windows.Forms.Button();
            this.btnItemLapBaoCaoDocGiaNoTien = new System.Windows.Forms.Button();
            this.btnItemLapBaoCaoSachTraTre = new System.Windows.Forms.Button();
            this.btnItemLapBaoCaoTheLoai = new System.Windows.Forms.Button();
            this.btnItemTiepNhanNhanVien = new System.Windows.Forms.Button();
            this.pnChucNangThuVien.SuspendLayout();
            this.SuspendLayout();
            // 
            // timerAnimationSlide
            // 
            this.timerAnimationSlide.Interval = 1;
            this.timerAnimationSlide.Tick += new System.EventHandler(this.timerAnimationSlide_Tick);
            // 
            // pnChucNangThuVien
            // 
            this.pnChucNangThuVien.Controls.Add(this.btnItemQuanLyNhanVien);
            this.pnChucNangThuVien.Controls.Add(this.btnItemLapBaoCaoDocGiaNoTien);
            this.pnChucNangThuVien.Controls.Add(this.btnItemLapBaoCaoSachTraTre);
            this.pnChucNangThuVien.Controls.Add(this.btnItemLapBaoCaoTheLoai);
            this.pnChucNangThuVien.Controls.Add(this.btnItemTiepNhanNhanVien);
            this.pnChucNangThuVien.Location = new System.Drawing.Point(0, 0);
            this.pnChucNangThuVien.Margin = new System.Windows.Forms.Padding(2);
            this.pnChucNangThuVien.Name = "pnChucNangThuVien";
            this.pnChucNangThuVien.Size = new System.Drawing.Size(185, 470);
            this.pnChucNangThuVien.TabIndex = 0;
            // 
            // btnItemQuanLyNhanVien
            // 
            this.btnItemQuanLyNhanVien.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemQuanLyNhanVien.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemQuanLyNhanVien.FlatAppearance.BorderSize = 0;
            this.btnItemQuanLyNhanVien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemQuanLyNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemQuanLyNhanVien.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemQuanLyNhanVien.Location = new System.Drawing.Point(0, 240);
            this.btnItemQuanLyNhanVien.Name = "btnItemQuanLyNhanVien";
            this.btnItemQuanLyNhanVien.Size = new System.Drawing.Size(185, 60);
            this.btnItemQuanLyNhanVien.TabIndex = 29;
            this.btnItemQuanLyNhanVien.Text = "Quản lý nhân viên";
            this.btnItemQuanLyNhanVien.UseVisualStyleBackColor = true;
            this.btnItemQuanLyNhanVien.Click += new System.EventHandler(this.btnItemQuanLyNhanVien_Click);
            // 
            // btnItemLapBaoCaoDocGiaNoTien
            // 
            this.btnItemLapBaoCaoDocGiaNoTien.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemLapBaoCaoDocGiaNoTien.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemLapBaoCaoDocGiaNoTien.FlatAppearance.BorderSize = 0;
            this.btnItemLapBaoCaoDocGiaNoTien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemLapBaoCaoDocGiaNoTien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemLapBaoCaoDocGiaNoTien.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemLapBaoCaoDocGiaNoTien.Location = new System.Drawing.Point(0, 180);
            this.btnItemLapBaoCaoDocGiaNoTien.Name = "btnItemLapBaoCaoDocGiaNoTien";
            this.btnItemLapBaoCaoDocGiaNoTien.Size = new System.Drawing.Size(185, 60);
            this.btnItemLapBaoCaoDocGiaNoTien.TabIndex = 30;
            this.btnItemLapBaoCaoDocGiaNoTien.Text = "Lập báo cáo thống kê đọc giả nợ tiền phạt";
            this.btnItemLapBaoCaoDocGiaNoTien.UseVisualStyleBackColor = true;
            this.btnItemLapBaoCaoDocGiaNoTien.Click += new System.EventHandler(this.btnItemLapBaoCaoDocGiaNoTien_Click);
            // 
            // btnItemLapBaoCaoSachTraTre
            // 
            this.btnItemLapBaoCaoSachTraTre.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemLapBaoCaoSachTraTre.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemLapBaoCaoSachTraTre.FlatAppearance.BorderSize = 0;
            this.btnItemLapBaoCaoSachTraTre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemLapBaoCaoSachTraTre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemLapBaoCaoSachTraTre.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemLapBaoCaoSachTraTre.Location = new System.Drawing.Point(0, 120);
            this.btnItemLapBaoCaoSachTraTre.Name = "btnItemLapBaoCaoSachTraTre";
            this.btnItemLapBaoCaoSachTraTre.Size = new System.Drawing.Size(185, 60);
            this.btnItemLapBaoCaoSachTraTre.TabIndex = 33;
            this.btnItemLapBaoCaoSachTraTre.Text = "Lập cáo cáo thống kê sách trả trễ";
            this.btnItemLapBaoCaoSachTraTre.UseVisualStyleBackColor = true;
            this.btnItemLapBaoCaoSachTraTre.Click += new System.EventHandler(this.btnItemLapBaoCaoSachTraTre_Click);
            // 
            // btnItemLapBaoCaoTheLoai
            // 
            this.btnItemLapBaoCaoTheLoai.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemLapBaoCaoTheLoai.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemLapBaoCaoTheLoai.FlatAppearance.BorderSize = 0;
            this.btnItemLapBaoCaoTheLoai.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemLapBaoCaoTheLoai.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemLapBaoCaoTheLoai.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemLapBaoCaoTheLoai.Location = new System.Drawing.Point(0, 60);
            this.btnItemLapBaoCaoTheLoai.Name = "btnItemLapBaoCaoTheLoai";
            this.btnItemLapBaoCaoTheLoai.Size = new System.Drawing.Size(185, 60);
            this.btnItemLapBaoCaoTheLoai.TabIndex = 32;
            this.btnItemLapBaoCaoTheLoai.Text = "Lập báo cáo mượn sách theo thể loại";
            this.btnItemLapBaoCaoTheLoai.UseVisualStyleBackColor = true;
            this.btnItemLapBaoCaoTheLoai.Click += new System.EventHandler(this.btnItemLapBaoCaoTheLoai_Click);
            // 
            // btnItemTiepNhanNhanVien
            // 
            this.btnItemTiepNhanNhanVien.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemTiepNhanNhanVien.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemTiepNhanNhanVien.FlatAppearance.BorderSize = 0;
            this.btnItemTiepNhanNhanVien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemTiepNhanNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemTiepNhanNhanVien.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemTiepNhanNhanVien.Location = new System.Drawing.Point(0, 0);
            this.btnItemTiepNhanNhanVien.Name = "btnItemTiepNhanNhanVien";
            this.btnItemTiepNhanNhanVien.Size = new System.Drawing.Size(185, 60);
            this.btnItemTiepNhanNhanVien.TabIndex = 31;
            this.btnItemTiepNhanNhanVien.Text = "Tiếp nhận nhân viên";
            this.btnItemTiepNhanNhanVien.UseVisualStyleBackColor = true;
            this.btnItemTiepNhanNhanVien.Click += new System.EventHandler(this.btnItemTiepNhanNhanVien_Click);
            // 
            // MenuChucNangThuVien
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(250)))));
            this.Controls.Add(this.pnChucNangThuVien);
            this.Name = "MenuChucNangThuVien";
            this.Size = new System.Drawing.Size(185, 470);
            this.Load += new System.EventHandler(this.MenuChucNangThuVien_Load);
            this.pnChucNangThuVien.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Timer timerAnimationSlide;
        private System.Windows.Forms.Panel pnChucNangThuVien;
        private System.Windows.Forms.Button btnItemQuanLyNhanVien;
        private System.Windows.Forms.Button btnItemLapBaoCaoDocGiaNoTien;
        private System.Windows.Forms.Button btnItemLapBaoCaoSachTraTre;
        private System.Windows.Forms.Button btnItemLapBaoCaoTheLoai;
        private System.Windows.Forms.Button btnItemTiepNhanNhanVien;
    }
}
