﻿namespace GUI.MenuChucNang
{
    partial class MenuChucNangExportImport
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnChucNangExportImport = new System.Windows.Forms.Panel();
            this.btnItemThoat = new System.Windows.Forms.Button();
            this.btnItemExportDocGia = new System.Windows.Forms.Button();
            this.btnItemExportSach = new System.Windows.Forms.Button();
            this.btnItemImportNhanVien = new System.Windows.Forms.Button();
            this.btnItemImportDocGia = new System.Windows.Forms.Button();
            this.btnItemImportSach = new System.Windows.Forms.Button();
            this.timerAnimationSlide = new System.Windows.Forms.Timer(this.components);
            this.pnChucNangExportImport.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnChucNangExportImport
            // 
            this.pnChucNangExportImport.Controls.Add(this.btnItemThoat);
            this.pnChucNangExportImport.Controls.Add(this.btnItemExportDocGia);
            this.pnChucNangExportImport.Controls.Add(this.btnItemExportSach);
            this.pnChucNangExportImport.Controls.Add(this.btnItemImportNhanVien);
            this.pnChucNangExportImport.Controls.Add(this.btnItemImportDocGia);
            this.pnChucNangExportImport.Controls.Add(this.btnItemImportSach);
            this.pnChucNangExportImport.Location = new System.Drawing.Point(0, 0);
            this.pnChucNangExportImport.Name = "pnChucNangExportImport";
            this.pnChucNangExportImport.Size = new System.Drawing.Size(185, 470);
            this.pnChucNangExportImport.TabIndex = 0;
            // 
            // btnItemThoat
            // 
            this.btnItemThoat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemThoat.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemThoat.FlatAppearance.BorderSize = 0;
            this.btnItemThoat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemThoat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemThoat.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemThoat.Location = new System.Drawing.Point(0, 300);
            this.btnItemThoat.Name = "btnItemThoat";
            this.btnItemThoat.Size = new System.Drawing.Size(185, 60);
            this.btnItemThoat.TabIndex = 20;
            this.btnItemThoat.Text = "Export nhân viên sang tập tin Excel";
            this.btnItemThoat.UseVisualStyleBackColor = true;
            // 
            // btnItemExportDocGia
            // 
            this.btnItemExportDocGia.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemExportDocGia.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemExportDocGia.FlatAppearance.BorderSize = 0;
            this.btnItemExportDocGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemExportDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemExportDocGia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemExportDocGia.Location = new System.Drawing.Point(0, 240);
            this.btnItemExportDocGia.Name = "btnItemExportDocGia";
            this.btnItemExportDocGia.Size = new System.Drawing.Size(185, 60);
            this.btnItemExportDocGia.TabIndex = 19;
            this.btnItemExportDocGia.Text = "Export đọc giả sang tập tin Excel";
            this.btnItemExportDocGia.UseVisualStyleBackColor = true;
            // 
            // btnItemExportSach
            // 
            this.btnItemExportSach.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemExportSach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemExportSach.FlatAppearance.BorderSize = 0;
            this.btnItemExportSach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemExportSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemExportSach.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemExportSach.Location = new System.Drawing.Point(0, 180);
            this.btnItemExportSach.Name = "btnItemExportSach";
            this.btnItemExportSach.Size = new System.Drawing.Size(185, 60);
            this.btnItemExportSach.TabIndex = 18;
            this.btnItemExportSach.Text = "Export sách sang tập tin Excel";
            this.btnItemExportSach.UseVisualStyleBackColor = true;
            // 
            // btnItemImportNhanVien
            // 
            this.btnItemImportNhanVien.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemImportNhanVien.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemImportNhanVien.FlatAppearance.BorderSize = 0;
            this.btnItemImportNhanVien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemImportNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemImportNhanVien.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemImportNhanVien.Location = new System.Drawing.Point(0, 120);
            this.btnItemImportNhanVien.Name = "btnItemImportNhanVien";
            this.btnItemImportNhanVien.Size = new System.Drawing.Size(185, 60);
            this.btnItemImportNhanVien.TabIndex = 17;
            this.btnItemImportNhanVien.Text = "Import nhân viên từ tập tin Excel";
            this.btnItemImportNhanVien.UseVisualStyleBackColor = true;
            // 
            // btnItemImportDocGia
            // 
            this.btnItemImportDocGia.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemImportDocGia.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemImportDocGia.FlatAppearance.BorderSize = 0;
            this.btnItemImportDocGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemImportDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemImportDocGia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemImportDocGia.Location = new System.Drawing.Point(0, 60);
            this.btnItemImportDocGia.Name = "btnItemImportDocGia";
            this.btnItemImportDocGia.Size = new System.Drawing.Size(185, 60);
            this.btnItemImportDocGia.TabIndex = 16;
            this.btnItemImportDocGia.Text = "Import độc giả từ tập tin Excel";
            this.btnItemImportDocGia.UseVisualStyleBackColor = true;
            // 
            // btnItemImportSach
            // 
            this.btnItemImportSach.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemImportSach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemImportSach.FlatAppearance.BorderSize = 0;
            this.btnItemImportSach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemImportSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemImportSach.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemImportSach.Location = new System.Drawing.Point(0, 0);
            this.btnItemImportSach.Name = "btnItemImportSach";
            this.btnItemImportSach.Size = new System.Drawing.Size(185, 60);
            this.btnItemImportSach.TabIndex = 15;
            this.btnItemImportSach.Text = "Import sách từ tập in Excel";
            this.btnItemImportSach.UseVisualStyleBackColor = true;
            // 
            // timerAnimationSlide
            // 
            this.timerAnimationSlide.Interval = 1;
            this.timerAnimationSlide.Tick += new System.EventHandler(this.timerAnimationSlide_Tick);
            // 
            // MenuChucNangExportImport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(250)))));
            this.Controls.Add(this.pnChucNangExportImport);
            this.Name = "MenuChucNangExportImport";
            this.Size = new System.Drawing.Size(185, 470);
            this.Load += new System.EventHandler(this.MenuChucNangExportImport_Load);
            this.pnChucNangExportImport.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnChucNangExportImport;
        private System.Windows.Forms.Button btnItemThoat;
        private System.Windows.Forms.Button btnItemExportDocGia;
        private System.Windows.Forms.Button btnItemExportSach;
        private System.Windows.Forms.Button btnItemImportNhanVien;
        private System.Windows.Forms.Button btnItemImportDocGia;
        private System.Windows.Forms.Button btnItemImportSach;
        private System.Windows.Forms.Timer timerAnimationSlide;
    }
}
