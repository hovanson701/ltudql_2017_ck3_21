﻿namespace GUI.MenuChucNang
{
    partial class MenuChucNangSach
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnChucNangSach = new System.Windows.Forms.Panel();
            this.btnItemThanhLySach = new System.Windows.Forms.Button();
            this.btnItemGhiNhanMatSach = new System.Windows.Forms.Button();
            this.btnItemNhanTraSach = new System.Windows.Forms.Button();
            this.btnItemChoMuonSach = new System.Windows.Forms.Button();
            this.btnItemTraCuuSach = new System.Windows.Forms.Button();
            this.btnItemTiepNhanSachMoi = new System.Windows.Forms.Button();
            this.timerAnimationSlide = new System.Windows.Forms.Timer(this.components);
            this.pnChucNangSach.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnChucNangSach
            // 
            this.pnChucNangSach.Controls.Add(this.btnItemThanhLySach);
            this.pnChucNangSach.Controls.Add(this.btnItemGhiNhanMatSach);
            this.pnChucNangSach.Controls.Add(this.btnItemNhanTraSach);
            this.pnChucNangSach.Controls.Add(this.btnItemChoMuonSach);
            this.pnChucNangSach.Controls.Add(this.btnItemTraCuuSach);
            this.pnChucNangSach.Controls.Add(this.btnItemTiepNhanSachMoi);
            this.pnChucNangSach.Location = new System.Drawing.Point(0, 0);
            this.pnChucNangSach.Name = "pnChucNangSach";
            this.pnChucNangSach.Size = new System.Drawing.Size(185, 470);
            this.pnChucNangSach.TabIndex = 0;
            // 
            // btnItemThanhLySach
            // 
            this.btnItemThanhLySach.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemThanhLySach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemThanhLySach.FlatAppearance.BorderSize = 0;
            this.btnItemThanhLySach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemThanhLySach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemThanhLySach.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemThanhLySach.Location = new System.Drawing.Point(0, 300);
            this.btnItemThanhLySach.Name = "btnItemThanhLySach";
            this.btnItemThanhLySach.Size = new System.Drawing.Size(185, 60);
            this.btnItemThanhLySach.TabIndex = 20;
            this.btnItemThanhLySach.Text = "Thanh lý sách";
            this.btnItemThanhLySach.UseVisualStyleBackColor = true;
            this.btnItemThanhLySach.Click += new System.EventHandler(this.btnItemThanhLySach_Click);
            // 
            // btnItemGhiNhanMatSach
            // 
            this.btnItemGhiNhanMatSach.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemGhiNhanMatSach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemGhiNhanMatSach.FlatAppearance.BorderSize = 0;
            this.btnItemGhiNhanMatSach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemGhiNhanMatSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemGhiNhanMatSach.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemGhiNhanMatSach.Location = new System.Drawing.Point(0, 240);
            this.btnItemGhiNhanMatSach.Name = "btnItemGhiNhanMatSach";
            this.btnItemGhiNhanMatSach.Size = new System.Drawing.Size(185, 60);
            this.btnItemGhiNhanMatSach.TabIndex = 19;
            this.btnItemGhiNhanMatSach.Text = "Ghi nhận mất sách";
            this.btnItemGhiNhanMatSach.UseVisualStyleBackColor = true;
            this.btnItemGhiNhanMatSach.Click += new System.EventHandler(this.btnItemGhiNhanMatSach_Click);
            // 
            // btnItemNhanTraSach
            // 
            this.btnItemNhanTraSach.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemNhanTraSach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemNhanTraSach.FlatAppearance.BorderSize = 0;
            this.btnItemNhanTraSach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemNhanTraSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemNhanTraSach.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemNhanTraSach.Location = new System.Drawing.Point(0, 180);
            this.btnItemNhanTraSach.Name = "btnItemNhanTraSach";
            this.btnItemNhanTraSach.Size = new System.Drawing.Size(185, 60);
            this.btnItemNhanTraSach.TabIndex = 18;
            this.btnItemNhanTraSach.Text = "Nhận trả sách";
            this.btnItemNhanTraSach.UseVisualStyleBackColor = true;
            this.btnItemNhanTraSach.Click += new System.EventHandler(this.btnItemNhanTraSach_Click);
            // 
            // btnItemChoMuonSach
            // 
            this.btnItemChoMuonSach.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemChoMuonSach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemChoMuonSach.FlatAppearance.BorderSize = 0;
            this.btnItemChoMuonSach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemChoMuonSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemChoMuonSach.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemChoMuonSach.Location = new System.Drawing.Point(0, 120);
            this.btnItemChoMuonSach.Name = "btnItemChoMuonSach";
            this.btnItemChoMuonSach.Size = new System.Drawing.Size(185, 60);
            this.btnItemChoMuonSach.TabIndex = 17;
            this.btnItemChoMuonSach.Text = "Cho mượn sách";
            this.btnItemChoMuonSach.UseVisualStyleBackColor = true;
            this.btnItemChoMuonSach.Click += new System.EventHandler(this.btnItemChoMuonSach_Click);
            // 
            // btnItemTraCuuSach
            // 
            this.btnItemTraCuuSach.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemTraCuuSach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemTraCuuSach.FlatAppearance.BorderSize = 0;
            this.btnItemTraCuuSach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemTraCuuSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemTraCuuSach.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemTraCuuSach.Location = new System.Drawing.Point(0, 60);
            this.btnItemTraCuuSach.Name = "btnItemTraCuuSach";
            this.btnItemTraCuuSach.Size = new System.Drawing.Size(185, 60);
            this.btnItemTraCuuSach.TabIndex = 16;
            this.btnItemTraCuuSach.Text = "Tra cứu sách";
            this.btnItemTraCuuSach.UseVisualStyleBackColor = true;
            this.btnItemTraCuuSach.Click += new System.EventHandler(this.btnItemTraCuuSach_Click);
            // 
            // btnItemTiepNhanSachMoi
            // 
            this.btnItemTiepNhanSachMoi.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemTiepNhanSachMoi.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemTiepNhanSachMoi.FlatAppearance.BorderSize = 0;
            this.btnItemTiepNhanSachMoi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemTiepNhanSachMoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemTiepNhanSachMoi.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemTiepNhanSachMoi.Location = new System.Drawing.Point(0, 0);
            this.btnItemTiepNhanSachMoi.Name = "btnItemTiepNhanSachMoi";
            this.btnItemTiepNhanSachMoi.Size = new System.Drawing.Size(185, 60);
            this.btnItemTiepNhanSachMoi.TabIndex = 15;
            this.btnItemTiepNhanSachMoi.Text = "Tiếp nhận sách mới";
            this.btnItemTiepNhanSachMoi.UseVisualStyleBackColor = true;
            this.btnItemTiepNhanSachMoi.Click += new System.EventHandler(this.btnItemTiepNhanSachMoi_Click);
            // 
            // timerAnimationSlide
            // 
            this.timerAnimationSlide.Interval = 1;
            this.timerAnimationSlide.Tick += new System.EventHandler(this.timerAnimationSlide_Tick);
            // 
            // MenuChucNangSach
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(250)))));
            this.Controls.Add(this.pnChucNangSach);
            this.Name = "MenuChucNangSach";
            this.Size = new System.Drawing.Size(185, 470);
            this.Load += new System.EventHandler(this.MenuChucNangSach_Load);
            this.pnChucNangSach.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnChucNangSach;
        private System.Windows.Forms.Button btnItemThanhLySach;
        private System.Windows.Forms.Button btnItemGhiNhanMatSach;
        private System.Windows.Forms.Button btnItemNhanTraSach;
        private System.Windows.Forms.Button btnItemChoMuonSach;
        private System.Windows.Forms.Button btnItemTraCuuSach;
        private System.Windows.Forms.Button btnItemTiepNhanSachMoi;
        private System.Windows.Forms.Timer timerAnimationSlide;
    }
}
