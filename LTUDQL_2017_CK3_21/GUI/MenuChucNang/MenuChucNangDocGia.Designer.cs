﻿namespace GUI.MenuChucNang
{
    partial class MenuChucNangDocGia
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnChucNangDocGia = new System.Windows.Forms.Panel();
            this.timerAnimationSlide = new System.Windows.Forms.Timer(this.components);
            this.btnItemQuanLyDocGia = new System.Windows.Forms.Button();
            this.btnItemLapPhieuThuTienPhat = new System.Windows.Forms.Button();
            this.btnItemLapTheDocGia = new System.Windows.Forms.Button();
            this.pnChucNangDocGia.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnChucNangDocGia
            // 
            this.pnChucNangDocGia.Controls.Add(this.btnItemQuanLyDocGia);
            this.pnChucNangDocGia.Controls.Add(this.btnItemLapPhieuThuTienPhat);
            this.pnChucNangDocGia.Controls.Add(this.btnItemLapTheDocGia);
            this.pnChucNangDocGia.Location = new System.Drawing.Point(0, 0);
            this.pnChucNangDocGia.Margin = new System.Windows.Forms.Padding(2);
            this.pnChucNangDocGia.Name = "pnChucNangDocGia";
            this.pnChucNangDocGia.Size = new System.Drawing.Size(185, 470);
            this.pnChucNangDocGia.TabIndex = 0;
            // 
            // timerAnimationSlide
            // 
            this.timerAnimationSlide.Interval = 1;
            this.timerAnimationSlide.Tick += new System.EventHandler(this.timerAnimationSlide_Tick);
            // 
            // btnItemQuanLyDocGia
            // 
            this.btnItemQuanLyDocGia.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemQuanLyDocGia.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemQuanLyDocGia.FlatAppearance.BorderSize = 0;
            this.btnItemQuanLyDocGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemQuanLyDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemQuanLyDocGia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemQuanLyDocGia.Location = new System.Drawing.Point(0, 98);
            this.btnItemQuanLyDocGia.Name = "btnItemQuanLyDocGia";
            this.btnItemQuanLyDocGia.Size = new System.Drawing.Size(185, 60);
            this.btnItemQuanLyDocGia.TabIndex = 26;
            this.btnItemQuanLyDocGia.Text = "Quản lý độc giả";
            this.btnItemQuanLyDocGia.UseVisualStyleBackColor = true;
            this.btnItemQuanLyDocGia.Click += new System.EventHandler(this.btnItemQuanLyDocGia_Click);
            // 
            // btnItemLapPhieuThuTienPhat
            // 
            this.btnItemLapPhieuThuTienPhat.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemLapPhieuThuTienPhat.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemLapPhieuThuTienPhat.FlatAppearance.BorderSize = 0;
            this.btnItemLapPhieuThuTienPhat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemLapPhieuThuTienPhat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemLapPhieuThuTienPhat.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemLapPhieuThuTienPhat.Location = new System.Drawing.Point(0, 49);
            this.btnItemLapPhieuThuTienPhat.Name = "btnItemLapPhieuThuTienPhat";
            this.btnItemLapPhieuThuTienPhat.Size = new System.Drawing.Size(185, 49);
            this.btnItemLapPhieuThuTienPhat.TabIndex = 28;
            this.btnItemLapPhieuThuTienPhat.Text = "Lập phiếu thu tiền phạt";
            this.btnItemLapPhieuThuTienPhat.UseVisualStyleBackColor = true;
            this.btnItemLapPhieuThuTienPhat.Click += new System.EventHandler(this.btnItemLapPhieuThuTienPhat_Click);
            // 
            // btnItemLapTheDocGia
            // 
            this.btnItemLapTheDocGia.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnItemLapTheDocGia.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnItemLapTheDocGia.FlatAppearance.BorderSize = 0;
            this.btnItemLapTheDocGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnItemLapTheDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnItemLapTheDocGia.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnItemLapTheDocGia.Location = new System.Drawing.Point(0, 0);
            this.btnItemLapTheDocGia.Name = "btnItemLapTheDocGia";
            this.btnItemLapTheDocGia.Size = new System.Drawing.Size(185, 49);
            this.btnItemLapTheDocGia.TabIndex = 27;
            this.btnItemLapTheDocGia.Text = "Lập thẻ đọc giả";
            this.btnItemLapTheDocGia.UseVisualStyleBackColor = true;
            this.btnItemLapTheDocGia.Click += new System.EventHandler(this.btnItemLapTheDocGia_Click);
            // 
            // MenuChucNangDocGia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(158)))), ((int)(((byte)(250)))));
            this.Controls.Add(this.pnChucNangDocGia);
            this.Name = "MenuChucNangDocGia";
            this.Size = new System.Drawing.Size(185, 470);
            this.Load += new System.EventHandler(this.MenuChucNangDocGia_Load);
            this.pnChucNangDocGia.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnChucNangDocGia;

        private System.Windows.Forms.Timer timerAnimationSlide;
        private System.Windows.Forms.Button btnItemQuanLyDocGia;
        private System.Windows.Forms.Button btnItemLapPhieuThuTienPhat;
        private System.Windows.Forms.Button btnItemLapTheDocGia;
    }
}
