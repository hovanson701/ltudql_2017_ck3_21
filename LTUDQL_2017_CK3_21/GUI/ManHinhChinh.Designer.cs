﻿namespace GUI
{
    partial class ManHinhChinh
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.pnTopMain = new System.Windows.Forms.Panel();
            this.panel13 = new System.Windows.Forms.Panel();
            this.flpnChucNangChinh = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnTiepNhanNhanVien = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnLapTheDocGia = new System.Windows.Forms.Button();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnTiepNhanSachMoi = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.btnTraCuuSach = new System.Windows.Forms.Button();
            this.panel5 = new System.Windows.Forms.Panel();
            this.btnChoMuonSach = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.btnNhanTraSach = new System.Windows.Forms.Button();
            this.panel7 = new System.Windows.Forms.Panel();
            this.btnLapPhieuThuTienPhat = new System.Windows.Forms.Button();
            this.panel8 = new System.Windows.Forms.Panel();
            this.btnGhiNhanMatSach = new System.Windows.Forms.Button();
            this.panel9 = new System.Windows.Forms.Panel();
            this.btnThanhLySach = new System.Windows.Forms.Button();
            this.panel10 = new System.Windows.Forms.Panel();
            this.btnBaoCaoTheoTheLoai = new System.Windows.Forms.Button();
            this.panel11 = new System.Windows.Forms.Panel();
            this.btnBaoCaoSachTraTre = new System.Windows.Forms.Button();
            this.panel12 = new System.Windows.Forms.Panel();
            this.btnBaoCaoDocGiaNoTienPhat = new System.Windows.Forms.Button();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.timerAnimation = new System.Windows.Forms.Timer(this.components);
            this.pnTopMain.SuspendLayout();
            this.panel13.SuspendLayout();
            this.flpnChucNangChinh.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel8.SuspendLayout();
            this.panel9.SuspendLayout();
            this.panel10.SuspendLayout();
            this.panel11.SuspendLayout();
            this.panel12.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnTopMain
            // 
            this.pnTopMain.Controls.Add(this.panel13);
            this.pnTopMain.Controls.Add(this.flowLayoutPanel5);
            this.pnTopMain.Controls.Add(this.flowLayoutPanel4);
            this.pnTopMain.Controls.Add(this.flowLayoutPanel3);
            this.pnTopMain.Controls.Add(this.flowLayoutPanel2);
            this.pnTopMain.Dock = System.Windows.Forms.DockStyle.Top;
            this.pnTopMain.Location = new System.Drawing.Point(0, 0);
            this.pnTopMain.Name = "pnTopMain";
            this.pnTopMain.Size = new System.Drawing.Size(680, 330);
            this.pnTopMain.TabIndex = 1;
            // 
            // panel13
            // 
            this.panel13.BackColor = System.Drawing.SystemColors.Control;
            this.panel13.Controls.Add(this.flpnChucNangChinh);
            this.panel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel13.Location = new System.Drawing.Point(22, 22);
            this.panel13.Name = "panel13";
            this.panel13.Size = new System.Drawing.Size(636, 286);
            this.panel13.TabIndex = 4;
            this.panel13.SizeChanged += new System.EventHandler(this.panel13_SizeChanged);
            // 
            // flpnChucNangChinh
            // 
            this.flpnChucNangChinh.BackColor = System.Drawing.SystemColors.Control;
            this.flpnChucNangChinh.Controls.Add(this.panel1);
            this.flpnChucNangChinh.Controls.Add(this.panel2);
            this.flpnChucNangChinh.Controls.Add(this.panel3);
            this.flpnChucNangChinh.Controls.Add(this.panel4);
            this.flpnChucNangChinh.Controls.Add(this.panel5);
            this.flpnChucNangChinh.Controls.Add(this.panel6);
            this.flpnChucNangChinh.Controls.Add(this.panel7);
            this.flpnChucNangChinh.Controls.Add(this.panel8);
            this.flpnChucNangChinh.Controls.Add(this.panel9);
            this.flpnChucNangChinh.Controls.Add(this.panel10);
            this.flpnChucNangChinh.Controls.Add(this.panel11);
            this.flpnChucNangChinh.Controls.Add(this.panel12);
            this.flpnChucNangChinh.Location = new System.Drawing.Point(0, 0);
            this.flpnChucNangChinh.Name = "flpnChucNangChinh";
            this.flpnChucNangChinh.Size = new System.Drawing.Size(636, 286);
            this.flpnChucNangChinh.TabIndex = 1;
            this.flpnChucNangChinh.SizeChanged += new System.EventHandler(this.flowLayoutPanel1_SizeChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.btnTiepNhanNhanVien);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.ForeColor = System.Drawing.SystemColors.ControlText;
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(153, 90);
            this.panel1.TabIndex = 0;
            // 
            // btnTiepNhanNhanVien
            // 
            this.btnTiepNhanNhanVien.BackColor = System.Drawing.Color.White;
            this.btnTiepNhanNhanVien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTiepNhanNhanVien.FlatAppearance.BorderColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnTiepNhanNhanVien.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnTiepNhanNhanVien.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnTiepNhanNhanVien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTiepNhanNhanVien.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTiepNhanNhanVien.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnTiepNhanNhanVien.Image = global::GUI.Properties.Resources.iconTiepNhanNhanVien2;
            this.btnTiepNhanNhanVien.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnTiepNhanNhanVien.Location = new System.Drawing.Point(0, 0);
            this.btnTiepNhanNhanVien.Name = "btnTiepNhanNhanVien";
            this.btnTiepNhanNhanVien.Size = new System.Drawing.Size(153, 90);
            this.btnTiepNhanNhanVien.TabIndex = 0;
            this.btnTiepNhanNhanVien.Text = "Tiếp nhận nhân viên";
            this.btnTiepNhanNhanVien.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTiepNhanNhanVien.UseVisualStyleBackColor = false;
            this.btnTiepNhanNhanVien.Click += new System.EventHandler(this.btnTiepNhanNhanVien_Click);
            this.btnTiepNhanNhanVien.MouseLeave += new System.EventHandler(this.btnTiepNhanNhanVien_MouseLeave);
            this.btnTiepNhanNhanVien.MouseHover += new System.EventHandler(this.btnTiepNhanNhanVien_MouseHover);
            this.btnTiepNhanNhanVien.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnTiepNhanNhanVien_MouseMove);
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.White;
            this.panel2.Controls.Add(this.btnLapTheDocGia);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(162, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(153, 90);
            this.panel2.TabIndex = 1;
            // 
            // btnLapTheDocGia
            // 
            this.btnLapTheDocGia.BackColor = System.Drawing.Color.White;
            this.btnLapTheDocGia.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLapTheDocGia.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLapTheDocGia.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnLapTheDocGia.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnLapTheDocGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLapTheDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLapTheDocGia.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnLapTheDocGia.Image = global::GUI.Properties.Resources.iconLapTheDocGia;
            this.btnLapTheDocGia.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnLapTheDocGia.Location = new System.Drawing.Point(0, 0);
            this.btnLapTheDocGia.Name = "btnLapTheDocGia";
            this.btnLapTheDocGia.Size = new System.Drawing.Size(153, 90);
            this.btnLapTheDocGia.TabIndex = 1;
            this.btnLapTheDocGia.Text = "Lập thẻ đọc giả";
            this.btnLapTheDocGia.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLapTheDocGia.UseVisualStyleBackColor = false;
            this.btnLapTheDocGia.Click += new System.EventHandler(this.btnLapTheDocGia_Click);
            this.btnLapTheDocGia.MouseLeave += new System.EventHandler(this.btnLapTheDocGia_MouseLeave);
            this.btnLapTheDocGia.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnLapTheDocGia_MouseMove);
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.White;
            this.panel3.Controls.Add(this.btnTiepNhanSachMoi);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(321, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(153, 90);
            this.panel3.TabIndex = 1;
            // 
            // btnTiepNhanSachMoi
            // 
            this.btnTiepNhanSachMoi.BackColor = System.Drawing.Color.White;
            this.btnTiepNhanSachMoi.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTiepNhanSachMoi.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnTiepNhanSachMoi.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnTiepNhanSachMoi.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnTiepNhanSachMoi.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTiepNhanSachMoi.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTiepNhanSachMoi.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnTiepNhanSachMoi.Image = global::GUI.Properties.Resources.iconTiepNhanSachMoi;
            this.btnTiepNhanSachMoi.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnTiepNhanSachMoi.Location = new System.Drawing.Point(0, 0);
            this.btnTiepNhanSachMoi.Name = "btnTiepNhanSachMoi";
            this.btnTiepNhanSachMoi.Size = new System.Drawing.Size(153, 90);
            this.btnTiepNhanSachMoi.TabIndex = 1;
            this.btnTiepNhanSachMoi.Text = "Tiếp nhận sách mới";
            this.btnTiepNhanSachMoi.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTiepNhanSachMoi.UseVisualStyleBackColor = false;
            this.btnTiepNhanSachMoi.Click += new System.EventHandler(this.btnTiepNhanSachMoi_Click);
            this.btnTiepNhanSachMoi.MouseLeave += new System.EventHandler(this.btnTiepNhanSachMoi_MouseLeave);
            this.btnTiepNhanSachMoi.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnTiepNhanSachMoi_MouseMove);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.White;
            this.panel4.Controls.Add(this.btnTraCuuSach);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(480, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(153, 90);
            this.panel4.TabIndex = 1;
            // 
            // btnTraCuuSach
            // 
            this.btnTraCuuSach.BackColor = System.Drawing.Color.White;
            this.btnTraCuuSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTraCuuSach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnTraCuuSach.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnTraCuuSach.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnTraCuuSach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTraCuuSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTraCuuSach.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnTraCuuSach.Image = global::GUI.Properties.Resources.iconTraCuuSach;
            this.btnTraCuuSach.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnTraCuuSach.Location = new System.Drawing.Point(0, 0);
            this.btnTraCuuSach.Name = "btnTraCuuSach";
            this.btnTraCuuSach.Size = new System.Drawing.Size(153, 90);
            this.btnTraCuuSach.TabIndex = 1;
            this.btnTraCuuSach.Text = "Tra cứu sách";
            this.btnTraCuuSach.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnTraCuuSach.UseVisualStyleBackColor = false;
            this.btnTraCuuSach.Click += new System.EventHandler(this.btnTraCuuSach_Click);
            this.btnTraCuuSach.MouseLeave += new System.EventHandler(this.btnTraCuuSach_MouseLeave);
            this.btnTraCuuSach.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnTraCuuSach_MouseMove);
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.White;
            this.panel5.Controls.Add(this.btnChoMuonSach);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel5.Location = new System.Drawing.Point(3, 99);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(153, 90);
            this.panel5.TabIndex = 1;
            // 
            // btnChoMuonSach
            // 
            this.btnChoMuonSach.BackColor = System.Drawing.Color.White;
            this.btnChoMuonSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnChoMuonSach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnChoMuonSach.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnChoMuonSach.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnChoMuonSach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnChoMuonSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnChoMuonSach.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnChoMuonSach.Image = global::GUI.Properties.Resources.iconChoMuonSach;
            this.btnChoMuonSach.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnChoMuonSach.Location = new System.Drawing.Point(0, 0);
            this.btnChoMuonSach.Name = "btnChoMuonSach";
            this.btnChoMuonSach.Size = new System.Drawing.Size(153, 90);
            this.btnChoMuonSach.TabIndex = 1;
            this.btnChoMuonSach.Text = "Cho mượn sách";
            this.btnChoMuonSach.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnChoMuonSach.UseVisualStyleBackColor = false;
            this.btnChoMuonSach.Click += new System.EventHandler(this.btnChoMuonSach_Click);
            this.btnChoMuonSach.MouseLeave += new System.EventHandler(this.btnChoMuonSach_MouseLeave);
            this.btnChoMuonSach.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnChoMuonSach_MouseMove);
            // 
            // panel6
            // 
            this.panel6.BackColor = System.Drawing.Color.White;
            this.panel6.Controls.Add(this.btnNhanTraSach);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(162, 99);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(153, 90);
            this.panel6.TabIndex = 1;
            // 
            // btnNhanTraSach
            // 
            this.btnNhanTraSach.BackColor = System.Drawing.Color.White;
            this.btnNhanTraSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnNhanTraSach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnNhanTraSach.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnNhanTraSach.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnNhanTraSach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNhanTraSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNhanTraSach.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnNhanTraSach.Image = global::GUI.Properties.Resources.iconNhanTraSach;
            this.btnNhanTraSach.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnNhanTraSach.Location = new System.Drawing.Point(0, 0);
            this.btnNhanTraSach.Name = "btnNhanTraSach";
            this.btnNhanTraSach.Size = new System.Drawing.Size(153, 90);
            this.btnNhanTraSach.TabIndex = 1;
            this.btnNhanTraSach.Text = "Nhận trả sách";
            this.btnNhanTraSach.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnNhanTraSach.UseVisualStyleBackColor = false;
            this.btnNhanTraSach.Click += new System.EventHandler(this.btnNhanTraSach_Click);
            this.btnNhanTraSach.MouseLeave += new System.EventHandler(this.btnNhanTraSach_MouseLeave);
            this.btnNhanTraSach.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnNhanTraSach_MouseMove);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.Color.White;
            this.panel7.Controls.Add(this.btnLapPhieuThuTienPhat);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel7.Location = new System.Drawing.Point(321, 99);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(153, 90);
            this.panel7.TabIndex = 1;
            // 
            // btnLapPhieuThuTienPhat
            // 
            this.btnLapPhieuThuTienPhat.BackColor = System.Drawing.Color.White;
            this.btnLapPhieuThuTienPhat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnLapPhieuThuTienPhat.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnLapPhieuThuTienPhat.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnLapPhieuThuTienPhat.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnLapPhieuThuTienPhat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLapPhieuThuTienPhat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLapPhieuThuTienPhat.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnLapPhieuThuTienPhat.Image = global::GUI.Properties.Resources.iconThuTienPhat;
            this.btnLapPhieuThuTienPhat.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnLapPhieuThuTienPhat.Location = new System.Drawing.Point(0, 0);
            this.btnLapPhieuThuTienPhat.Name = "btnLapPhieuThuTienPhat";
            this.btnLapPhieuThuTienPhat.Size = new System.Drawing.Size(153, 90);
            this.btnLapPhieuThuTienPhat.TabIndex = 1;
            this.btnLapPhieuThuTienPhat.Text = "Phiếu thu tiền phạt";
            this.btnLapPhieuThuTienPhat.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnLapPhieuThuTienPhat.UseVisualStyleBackColor = false;
            this.btnLapPhieuThuTienPhat.Click += new System.EventHandler(this.btnLapPhieuThuTienPhat_Click);
            this.btnLapPhieuThuTienPhat.MouseLeave += new System.EventHandler(this.btnLapPhieuThuTienPhat_MouseLeave);
            this.btnLapPhieuThuTienPhat.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnLapPhieuThuTienPhat_MouseMove);
            // 
            // panel8
            // 
            this.panel8.BackColor = System.Drawing.Color.White;
            this.panel8.Controls.Add(this.btnGhiNhanMatSach);
            this.panel8.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel8.Location = new System.Drawing.Point(480, 99);
            this.panel8.Name = "panel8";
            this.panel8.Size = new System.Drawing.Size(153, 90);
            this.panel8.TabIndex = 1;
            // 
            // btnGhiNhanMatSach
            // 
            this.btnGhiNhanMatSach.BackColor = System.Drawing.Color.White;
            this.btnGhiNhanMatSach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnGhiNhanMatSach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnGhiNhanMatSach.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnGhiNhanMatSach.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnGhiNhanMatSach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnGhiNhanMatSach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnGhiNhanMatSach.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnGhiNhanMatSach.Image = global::GUI.Properties.Resources.iconMatSach;
            this.btnGhiNhanMatSach.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnGhiNhanMatSach.Location = new System.Drawing.Point(0, 0);
            this.btnGhiNhanMatSach.Name = "btnGhiNhanMatSach";
            this.btnGhiNhanMatSach.Size = new System.Drawing.Size(153, 90);
            this.btnGhiNhanMatSach.TabIndex = 1;
            this.btnGhiNhanMatSach.Text = "Ghi nhận mất sách";
            this.btnGhiNhanMatSach.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnGhiNhanMatSach.UseVisualStyleBackColor = false;
            this.btnGhiNhanMatSach.Click += new System.EventHandler(this.btnGhiNhanMatSach_Click);
            this.btnGhiNhanMatSach.MouseLeave += new System.EventHandler(this.btnGhiNhanMatSach_MouseLeave);
            this.btnGhiNhanMatSach.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnGhiNhanMatSach_MouseMove);
            // 
            // panel9
            // 
            this.panel9.BackColor = System.Drawing.Color.White;
            this.panel9.Controls.Add(this.btnThanhLySach);
            this.panel9.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel9.Location = new System.Drawing.Point(3, 195);
            this.panel9.Name = "panel9";
            this.panel9.Size = new System.Drawing.Size(153, 90);
            this.panel9.TabIndex = 1;
            // 
            // btnThanhLySach
            // 
            this.btnThanhLySach.BackColor = System.Drawing.Color.White;
            this.btnThanhLySach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnThanhLySach.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnThanhLySach.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnThanhLySach.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnThanhLySach.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnThanhLySach.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnThanhLySach.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnThanhLySach.Image = global::GUI.Properties.Resources.iconThanhLySach;
            this.btnThanhLySach.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnThanhLySach.Location = new System.Drawing.Point(0, 0);
            this.btnThanhLySach.Name = "btnThanhLySach";
            this.btnThanhLySach.Size = new System.Drawing.Size(153, 90);
            this.btnThanhLySach.TabIndex = 1;
            this.btnThanhLySach.Text = "Thanh lý sách";
            this.btnThanhLySach.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnThanhLySach.UseVisualStyleBackColor = false;
            this.btnThanhLySach.Click += new System.EventHandler(this.btnThanhLySach_Click);
            this.btnThanhLySach.MouseLeave += new System.EventHandler(this.btnThanhLySach_MouseLeave);
            this.btnThanhLySach.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnThanhLySach_MouseMove);
            // 
            // panel10
            // 
            this.panel10.BackColor = System.Drawing.Color.White;
            this.panel10.Controls.Add(this.btnBaoCaoTheoTheLoai);
            this.panel10.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel10.Location = new System.Drawing.Point(162, 195);
            this.panel10.Name = "panel10";
            this.panel10.Size = new System.Drawing.Size(153, 90);
            this.panel10.TabIndex = 1;
            // 
            // btnBaoCaoTheoTheLoai
            // 
            this.btnBaoCaoTheoTheLoai.BackColor = System.Drawing.Color.White;
            this.btnBaoCaoTheoTheLoai.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBaoCaoTheoTheLoai.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBaoCaoTheoTheLoai.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnBaoCaoTheoTheLoai.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnBaoCaoTheoTheLoai.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBaoCaoTheoTheLoai.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBaoCaoTheoTheLoai.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnBaoCaoTheoTheLoai.Image = global::GUI.Properties.Resources.iconBCMuonTheoTheLoai;
            this.btnBaoCaoTheoTheLoai.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBaoCaoTheoTheLoai.Location = new System.Drawing.Point(0, 0);
            this.btnBaoCaoTheoTheLoai.Name = "btnBaoCaoTheoTheLoai";
            this.btnBaoCaoTheoTheLoai.Size = new System.Drawing.Size(153, 90);
            this.btnBaoCaoTheoTheLoai.TabIndex = 1;
            this.btnBaoCaoTheoTheLoai.Text = "Báo cáo mượn sách theo thể loại";
            this.btnBaoCaoTheoTheLoai.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBaoCaoTheoTheLoai.UseVisualStyleBackColor = false;
            this.btnBaoCaoTheoTheLoai.Click += new System.EventHandler(this.btnBaoCaoTheoTheLoai_Click);
            this.btnBaoCaoTheoTheLoai.MouseLeave += new System.EventHandler(this.btnBaoCaoTheoTheLoai_MouseLeave);
            this.btnBaoCaoTheoTheLoai.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnBaoCaoTheoTheLoai_MouseMove);
            // 
            // panel11
            // 
            this.panel11.BackColor = System.Drawing.Color.White;
            this.panel11.Controls.Add(this.btnBaoCaoSachTraTre);
            this.panel11.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel11.Location = new System.Drawing.Point(321, 195);
            this.panel11.Name = "panel11";
            this.panel11.Size = new System.Drawing.Size(153, 90);
            this.panel11.TabIndex = 1;
            // 
            // btnBaoCaoSachTraTre
            // 
            this.btnBaoCaoSachTraTre.BackColor = System.Drawing.Color.White;
            this.btnBaoCaoSachTraTre.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBaoCaoSachTraTre.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBaoCaoSachTraTre.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnBaoCaoSachTraTre.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnBaoCaoSachTraTre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBaoCaoSachTraTre.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBaoCaoSachTraTre.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnBaoCaoSachTraTre.Image = global::GUI.Properties.Resources.iconBCSachTraTre;
            this.btnBaoCaoSachTraTre.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBaoCaoSachTraTre.Location = new System.Drawing.Point(0, 0);
            this.btnBaoCaoSachTraTre.Name = "btnBaoCaoSachTraTre";
            this.btnBaoCaoSachTraTre.Size = new System.Drawing.Size(153, 90);
            this.btnBaoCaoSachTraTre.TabIndex = 1;
            this.btnBaoCaoSachTraTre.Text = "Báo cáo sách trả trễ";
            this.btnBaoCaoSachTraTre.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBaoCaoSachTraTre.UseVisualStyleBackColor = false;
            this.btnBaoCaoSachTraTre.Click += new System.EventHandler(this.btnBaoCaoSachTraTre_Click);
            this.btnBaoCaoSachTraTre.MouseLeave += new System.EventHandler(this.btnBaoCaoSachTraTre_MouseLeave);
            this.btnBaoCaoSachTraTre.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnBaoCaoSachTraTre_MouseMove);
            // 
            // panel12
            // 
            this.panel12.BackColor = System.Drawing.Color.White;
            this.panel12.Controls.Add(this.btnBaoCaoDocGiaNoTienPhat);
            this.panel12.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel12.Location = new System.Drawing.Point(480, 195);
            this.panel12.Name = "panel12";
            this.panel12.Size = new System.Drawing.Size(153, 90);
            this.panel12.TabIndex = 1;
            // 
            // btnBaoCaoDocGiaNoTienPhat
            // 
            this.btnBaoCaoDocGiaNoTienPhat.BackColor = System.Drawing.Color.White;
            this.btnBaoCaoDocGiaNoTienPhat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnBaoCaoDocGiaNoTienPhat.FlatAppearance.BorderColor = System.Drawing.Color.White;
            this.btnBaoCaoDocGiaNoTienPhat.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White;
            this.btnBaoCaoDocGiaNoTienPhat.FlatAppearance.MouseOverBackColor = System.Drawing.Color.White;
            this.btnBaoCaoDocGiaNoTienPhat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBaoCaoDocGiaNoTienPhat.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnBaoCaoDocGiaNoTienPhat.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnBaoCaoDocGiaNoTienPhat.Image = global::GUI.Properties.Resources.iconBCDocGiaNoTienPhat;
            this.btnBaoCaoDocGiaNoTienPhat.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnBaoCaoDocGiaNoTienPhat.Location = new System.Drawing.Point(0, 0);
            this.btnBaoCaoDocGiaNoTienPhat.Name = "btnBaoCaoDocGiaNoTienPhat";
            this.btnBaoCaoDocGiaNoTienPhat.Size = new System.Drawing.Size(153, 90);
            this.btnBaoCaoDocGiaNoTienPhat.TabIndex = 1;
            this.btnBaoCaoDocGiaNoTienPhat.Text = "Báo cáo đọc giả nợ tiền phạt";
            this.btnBaoCaoDocGiaNoTienPhat.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnBaoCaoDocGiaNoTienPhat.UseVisualStyleBackColor = false;
            this.btnBaoCaoDocGiaNoTienPhat.Click += new System.EventHandler(this.btnBaoCaoDocGiaNoTienPhat_Click);
            this.btnBaoCaoDocGiaNoTienPhat.MouseLeave += new System.EventHandler(this.btnBaoCaoDocGiaNoTienPhat_MouseLeave);
            this.btnBaoCaoDocGiaNoTienPhat.MouseMove += new System.Windows.Forms.MouseEventHandler(this.btnBaoCaoDocGiaNoTienPhat_MouseMove);
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Top;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(22, 0);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(636, 22);
            this.flowLayoutPanel5.TabIndex = 3;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(22, 308);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(636, 22);
            this.flowLayoutPanel4.TabIndex = 3;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Left;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(22, 330);
            this.flowLayoutPanel3.TabIndex = 2;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(658, 0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(22, 330);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // timerAnimation
            // 
            this.timerAnimation.Interval = 5;
            this.timerAnimation.Tick += new System.EventHandler(this.timerAnimation_Tick);
            // 
            // ManHinhChinh
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.Controls.Add(this.pnTopMain);
            this.Name = "ManHinhChinh";
            this.Size = new System.Drawing.Size(680, 530);
            this.Load += new System.EventHandler(this.ManHinhChinh_Load);
            this.pnTopMain.ResumeLayout(false);
            this.panel13.ResumeLayout(false);
            this.flpnChucNangChinh.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel5.ResumeLayout(false);
            this.panel6.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.panel8.ResumeLayout(false);
            this.panel9.ResumeLayout(false);
            this.panel10.ResumeLayout(false);
            this.panel11.ResumeLayout(false);
            this.panel12.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnTopMain;
        private System.Windows.Forms.FlowLayoutPanel flpnChucNangChinh;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Panel panel8;
        private System.Windows.Forms.Panel panel9;
        private System.Windows.Forms.Panel panel10;
        private System.Windows.Forms.Panel panel11;
        private System.Windows.Forms.Panel panel12;
        private System.Windows.Forms.Button btnTiepNhanNhanVien;
        private System.Windows.Forms.Button btnLapTheDocGia;
        private System.Windows.Forms.Button btnTiepNhanSachMoi;
        private System.Windows.Forms.Button btnTraCuuSach;
        private System.Windows.Forms.Button btnChoMuonSach;
        private System.Windows.Forms.Button btnNhanTraSach;
        private System.Windows.Forms.Button btnLapPhieuThuTienPhat;
        private System.Windows.Forms.Button btnGhiNhanMatSach;
        private System.Windows.Forms.Button btnThanhLySach;
        private System.Windows.Forms.Button btnBaoCaoTheoTheLoai;
        private System.Windows.Forms.Button btnBaoCaoSachTraTre;
        private System.Windows.Forms.Button btnBaoCaoDocGiaNoTienPhat;
        private System.Windows.Forms.Panel panel13;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.Timer timerAnimation;
    }
}
