﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static GUI.ManHinhThucDonForm;

namespace GUI
{
    public partial class ManHinhChinh : UserControl
    {
        public HamChuyenTrang ChuyenTrangChucNang;

        int Speed = 1;

        int oldWidthFlowForm;
        int oldHeightFlowForm;

        int oldWidthFlowFormWrapper;
        int oldHeightFlowFormWrapper;

        public ManHinhChinh(/*HamChuyenTrang ChuyenTrangChucNang*/)
        {
            InitializeComponent();

            oldWidthFlowForm = flpnChucNangChinh.Width;
            oldHeightFlowForm = flpnChucNangChinh.Height;

            oldWidthFlowFormWrapper = panel13.Width;
            oldHeightFlowFormWrapper = panel13.Height;

            this.Dock = DockStyle.Top;

            //this.ChuyenTrangChucNang = ChuyenTrangChucNang;
        }


        public ManHinhChinh(HamChuyenTrang ChuyenTrangChucNang)
        {
            InitializeComponent();

            oldWidthFlowForm = flpnChucNangChinh.Width;
            oldHeightFlowForm = flpnChucNangChinh.Height;

            oldWidthFlowFormWrapper = panel13.Width;
            oldHeightFlowFormWrapper = panel13.Height;

            this.Dock = DockStyle.Top;

            this.ChuyenTrangChucNang = ChuyenTrangChucNang;
        }

        private void flowLayoutPanel1_SizeChanged(object sender, EventArgs e)
        {

            int IncreaseWidth = flpnChucNangChinh.Width - oldWidthFlowForm;
            int IncreaseHeight = flpnChucNangChinh.Height - oldHeightFlowForm;

            foreach (Control itemControl in flpnChucNangChinh.Controls)
            {
                if (itemControl is Panel)
                {
                    itemControl.Width += IncreaseWidth / 4;
                    itemControl.Height += IncreaseHeight / 4;
                }
                
            }


            oldWidthFlowForm = flpnChucNangChinh.Width;
            oldHeightFlowForm = flpnChucNangChinh.Height;
        }

        private void timerAnimation_Tick(object sender, EventArgs e)
        {
            int X = flpnChucNangChinh.Location.X;
            int Y = flpnChucNangChinh.Location.Y - Speed;

            if (Y < 0 || Speed < 0)
            {
                Y = 0;
                timerAnimation.Stop();
            }

            flpnChucNangChinh.Location = new Point(X, Y);

            if (Y >= panel13.Height / 2)
                Speed++;
            else
            {
                Speed--;
                timerAnimation.Interval = 30;
            }
        }

        private void ManHinhChinh_Load(object sender, EventArgs e)
        {
            flpnChucNangChinh.Location = new Point(0, panel13.Height);
            timerAnimation.Start();
        }

        private void panel13_SizeChanged(object sender, EventArgs e)
        {
            int IncreaseWidth = panel13.Width - oldWidthFlowFormWrapper;
            int IncreaseHeight = panel13.Height - oldHeightFlowFormWrapper;

            flpnChucNangChinh.Width += IncreaseWidth;
            flpnChucNangChinh.Height += IncreaseHeight;

            oldWidthFlowFormWrapper = panel13.Width;
            oldHeightFlowFormWrapper = panel13.Height;

        }

        private void btnTiepNhanNhanVien_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(1);
        }

        private void btnLapTheDocGia_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(2);
        }

        private void btnTiepNhanSachMoi_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(3);
        }

        private void btnTraCuuSach_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(4);
        }

        private void btnChoMuonSach_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(5);
        }

        private void btnNhanTraSach_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(6);
        }

        private void btnLapPhieuThuTienPhat_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(7);
        }

        private void btnGhiNhanMatSach_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(8);
        }

        private void btnThanhLySach_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(9);
        }

        private void btnBaoCaoTheoTheLoai_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(10);
        }

        private void btnBaoCaoSachTraTre_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(11);
        }

        private void btnBaoCaoDocGiaNoTienPhat_Click(object sender, EventArgs e)
        {
            this.ChuyenTrangChucNang(12);
        }

        private void btnTiepNhanNhanVien_MouseHover(object sender, EventArgs e)
        {
            //btnTiepNhanNhanVien.ForeColor = Color.Blue;
            //btnTiepNhanNhanVien.FlatAppearance.BorderColor = Color.FromArgb(0, 158, 250);
        }

        private void btnTiepNhanNhanVien_MouseLeave(object sender, EventArgs e)
        {
            btnTiepNhanNhanVien.ForeColor = Color.Black;
            btnTiepNhanNhanVien.FlatAppearance.BorderColor = Color.White;
        }

        private void btnTiepNhanNhanVien_MouseMove(object sender, MouseEventArgs e)
        {
            btnTiepNhanNhanVien.ForeColor = Color.FromArgb(250, 115, 0);
            btnTiepNhanNhanVien.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnLapTheDocGia_MouseMove(object sender, MouseEventArgs e)
        {
            btnLapTheDocGia.ForeColor = Color.FromArgb(250, 115, 0);
            btnLapTheDocGia.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnLapTheDocGia_MouseLeave(object sender, EventArgs e)
        {
            btnLapTheDocGia.ForeColor = Color.Black;
            btnLapTheDocGia.FlatAppearance.BorderColor = Color.White;
        }

        private void btnTiepNhanSachMoi_MouseMove(object sender, MouseEventArgs e)
        {
            btnTiepNhanSachMoi.ForeColor = Color.FromArgb(250, 115, 0);
            btnTiepNhanSachMoi.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnTiepNhanSachMoi_MouseLeave(object sender, EventArgs e)
        {
            btnTiepNhanSachMoi.ForeColor = Color.Black;
            btnTiepNhanSachMoi.FlatAppearance.BorderColor = Color.White;
        }

        private void btnTraCuuSach_MouseMove(object sender, MouseEventArgs e)
        {
            btnTraCuuSach.ForeColor = Color.FromArgb(250, 115, 0);
            btnTraCuuSach.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnTraCuuSach_MouseLeave(object sender, EventArgs e)
        {
            btnTraCuuSach.ForeColor = Color.Black;
            btnTraCuuSach.FlatAppearance.BorderColor = Color.White;
        }

        private void btnChoMuonSach_MouseMove(object sender, MouseEventArgs e)
        {
            btnChoMuonSach.ForeColor = Color.FromArgb(250, 115, 0);
            btnChoMuonSach.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnChoMuonSach_MouseLeave(object sender, EventArgs e)
        {
            btnChoMuonSach.ForeColor = Color.Black;
            btnChoMuonSach.FlatAppearance.BorderColor = Color.White;
        }

        private void btnNhanTraSach_MouseMove(object sender, MouseEventArgs e)
        {
            btnNhanTraSach.ForeColor = Color.FromArgb(250, 115, 0);
            btnNhanTraSach.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnNhanTraSach_MouseLeave(object sender, EventArgs e)
        {
            btnNhanTraSach.ForeColor = Color.Black;
            btnNhanTraSach.FlatAppearance.BorderColor = Color.White;
        }

        private void btnLapPhieuThuTienPhat_MouseMove(object sender, MouseEventArgs e)
        {
            btnLapPhieuThuTienPhat.ForeColor = Color.FromArgb(250, 115, 0);
            btnLapPhieuThuTienPhat.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnLapPhieuThuTienPhat_MouseLeave(object sender, EventArgs e)
        {
            btnLapPhieuThuTienPhat.ForeColor = Color.Black;
            btnLapPhieuThuTienPhat.FlatAppearance.BorderColor = Color.White;
        }

        private void btnGhiNhanMatSach_MouseMove(object sender, MouseEventArgs e)
        {
            btnGhiNhanMatSach.ForeColor = Color.FromArgb(250, 115, 0);
            btnGhiNhanMatSach.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnGhiNhanMatSach_MouseLeave(object sender, EventArgs e)
        {
            btnGhiNhanMatSach.ForeColor = Color.Black;
            btnGhiNhanMatSach.FlatAppearance.BorderColor = Color.White;
        }

        private void btnThanhLySach_MouseMove(object sender, MouseEventArgs e)
        {
            btnThanhLySach.ForeColor = Color.FromArgb(250, 115, 0);
            btnThanhLySach.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnThanhLySach_MouseLeave(object sender, EventArgs e)
        {
            btnThanhLySach.ForeColor = Color.Black;
            btnThanhLySach.FlatAppearance.BorderColor = Color.White;
        }

        private void btnBaoCaoTheoTheLoai_MouseMove(object sender, MouseEventArgs e)
        {
            btnBaoCaoTheoTheLoai.ForeColor = Color.FromArgb(250, 115, 0);
            btnBaoCaoTheoTheLoai.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnBaoCaoTheoTheLoai_MouseLeave(object sender, EventArgs e)
        {
            btnBaoCaoTheoTheLoai.ForeColor = Color.Black;
            btnBaoCaoTheoTheLoai.FlatAppearance.BorderColor = Color.White;
        }

        private void btnBaoCaoSachTraTre_MouseMove(object sender, MouseEventArgs e)
        {
            btnBaoCaoSachTraTre.ForeColor = Color.FromArgb(250, 115, 0);
            btnBaoCaoSachTraTre.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnBaoCaoSachTraTre_MouseLeave(object sender, EventArgs e)
        {
            btnBaoCaoSachTraTre.ForeColor = Color.Black;
            btnBaoCaoSachTraTre.FlatAppearance.BorderColor = Color.White;
        }

        private void btnBaoCaoDocGiaNoTienPhat_MouseMove(object sender, MouseEventArgs e)
        {
            btnBaoCaoDocGiaNoTienPhat.ForeColor = Color.FromArgb(250, 115, 0);
            btnBaoCaoDocGiaNoTienPhat.FlatAppearance.BorderColor = Color.FromArgb(255, 155, 55);
        }

        private void btnBaoCaoDocGiaNoTienPhat_MouseLeave(object sender, EventArgs e)
        {
            btnBaoCaoDocGiaNoTienPhat.ForeColor = Color.Black;
            btnBaoCaoDocGiaNoTienPhat.FlatAppearance.BorderColor = Color.White;
        }
    }
}
