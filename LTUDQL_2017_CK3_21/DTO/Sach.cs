﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class Sach
    {
        public string Ma { get; set; }
        public string Ten { get; set; }
        public DateTime NgayXuatBan { get; set; }
        public string MaTacGia { get; set; }
        public string MaTheLoai { get; set; }
        public string MaMXB { get; set; }
        public double GiaTien { get; set; }
        public string ViTri { get; set; }
        public string MaTinhTrang { get; set; }
        public string MaNVTiepNhan { get; set; }
        public DateTime NgayTiepNhan { get; set; }
    }
}
