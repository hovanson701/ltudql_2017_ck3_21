﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class NhanVienDTO
    {
        private string _MaNhanVien;
        private string _HoTenNV;
        private string _DiaChi;
        private string _NgaySinh;
        private string _BangCap;
        private string _ChucVu;
        private string _BoPhan;
        private string _DienThoai;
        private string _MaTTNV;

        public string MaNhanVien { get => _MaNhanVien; set => _MaNhanVien = value; }
        public string HoTenNV { get => _HoTenNV; set => _HoTenNV = value; }
        public string DiaChi { get => _DiaChi; set => _DiaChi = value; }
        public string NgaySinh { get => _NgaySinh; set => _NgaySinh = value; }
        public string BangCap { get => _BangCap; set => _BangCap = value; }
        public string ChucVu { get => _ChucVu; set => _ChucVu = value; }
        public string BoPhan { get => _BoPhan; set => _BoPhan = value; }
        public string DienThoai { get => _DienThoai; set => _DienThoai = value; }
        public string MaTTNV { get => _MaTTNV; set => _MaTTNV = value; }
    }
}
