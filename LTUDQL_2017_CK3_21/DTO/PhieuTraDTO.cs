﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PhieuTraDTO
    {
        string maPT, maPM;
        DateTime ngayTra;
        float tienPhatKyNay, tienNo, soNgayTraTre;

        public string MaPT { get => maPT; set => maPT = value; }
        public string MaPM { get => maPM; set => maPM = value; }
        public DateTime NgayTra { get => ngayTra; set => ngayTra = value; }
        public float TienPhatKyNay { get => tienPhatKyNay; set => tienPhatKyNay = value; }
        public float TienNo { get => tienNo; set => tienNo = value; }
        public float SoNgayTraTre { get => soNgayTraTre; set => soNgayTraTre = value; }
    }
}
