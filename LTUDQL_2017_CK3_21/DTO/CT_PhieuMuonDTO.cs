﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class CT_PhieuMuonDTO
    {
        string maPM, maSach;

        public string MaPM { get => maPM; set => maPM = value; }
        public string MaSach { get => maSach; set => maSach = value; }
    }
}
