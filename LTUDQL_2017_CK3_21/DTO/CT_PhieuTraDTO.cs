﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class CT_PhieuTraDTO
    {
        string maPT, maSach;
        DateTime ngayMuon;
        int soNgayMuon;
        float tienPhat;

        public string MaPT { get => maPT; set => maPT = value; }
        public DateTime NgayMuon { get => ngayMuon; set => ngayMuon = value; }
        public int SoNgayMuon { get => soNgayMuon; set => soNgayMuon = value; }
        public float TienPhat { get => tienPhat; set => tienPhat = value; }
        public string MaSach { get => maSach; set => maSach = value; }
    }
}
