﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class DocGiaDTO
    {
        private string _MaDocGia;
        private string _HoTenDG;
        private string _NgaySinh;
        private string _DiaChi;
        private string _Email;
        private float _TongNo;
        private string _NgayLapThe;
        private string _NgayHetHan;
        private string _NVLapThe;
        private string _MaLoaiDG;
        private string _TTDG;

        public string MaDocGia { get => _MaDocGia; set => _MaDocGia = value; }
        public string HoTenDG { get => _HoTenDG; set => _HoTenDG = value; }
        public string DiaChi { get => _DiaChi; set => _DiaChi = value; }
        public string Email { get => _Email; set => _Email = value; }
        public float TongNo { get => _TongNo; set => _TongNo = value; }
        public string NVLapThe { get => _NVLapThe; set => _NVLapThe = value; }
        public string MaLoaiDG { get => _MaLoaiDG; set => _MaLoaiDG = value; }
        public string TTDG { get => _TTDG; set => _TTDG = value; }
        public string NgayLapThe { get => _NgayLapThe; set => _NgayLapThe = value; }
        public string NgayHetHan { get => _NgayHetHan; set => _NgayHetHan = value; }
        public string NgaySinh { get => _NgaySinh; set => _NgaySinh = value; }
    }
}
