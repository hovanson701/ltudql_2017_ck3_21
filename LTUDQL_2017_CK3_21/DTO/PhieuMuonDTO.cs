﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    public class PhieuMuonDTO
    {
        string maPM, maDG;
        DateTime ngayMuon, ngayHetHan;
        public string MaPM { get => maPM; set => maPM = value; }
        public DateTime NgayMuon { get => ngayMuon; set => ngayMuon = value; }
        public string MaDG { get => maDG; set => maDG = value; }
        public DateTime NgayHetHan { get => ngayHetHan; set => ngayHetHan = value; }
    }
}
